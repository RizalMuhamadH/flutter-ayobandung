import 'package:shared_preferences/shared_preferences.dart';

class SharedPreference{
  static const NOTIFICATION = "notification";
  static setStateNotification(bool status) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(NOTIFICATION, status);
  }

  static Future<bool> getStateNotification() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool on = prefs.getBool(NOTIFICATION);
    return on;
  }
}