class Other{
  static const URL_IMAGE = "https://www.ayobandung.com/images-bandung/post/articles/";
  static const URL_PHOTOS = "https://www.ayobandung.com/images-bandung/post/photos/";
  static const BASE_URL = "https://www.ayobandung.com/api_mob_new/";
  static const URL_WEBSITE = "https://www.ayobandung.com/read/";
  static const URL_READ_PHOTO = "https://www.ayobandung.com/view/";
}