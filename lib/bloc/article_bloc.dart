import 'dart:async';
import 'dart:convert';
import 'package:ayobandung/models/all_model.dart';
import 'package:ayobandung/utility/other.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:rxdart/rxdart.dart';
import './bloc.dart';

class ArticleBloc extends Bloc<ArticleEvent, ArticleState> {
  @override
  ArticleState get initialState => InitialArticleState();

  @override
  Stream<ArticleState> mapEventToState(
    ArticleEvent event,
  ) async* {
    if (event is getAll) {
      yield InitialArticleLoading();
      // ignore: close_sinks
//      final article = PublishSubject<AllModel>();
//      Observable<AllModel> allArticle() => article.stream;
      final all = await _fetchGetAll();
//      article.sink.add(all);
      yield InitialArticleLoaded(all);
    }
  }

  Future<AllModel> _fetchGetAll() async {
    Response response = await Dio().get("${Other.BASE_URL}getAll");
    var decodeJson = jsonDecode(response.toString());
    return AllModel.fromJson(decodeJson);
  }
}
