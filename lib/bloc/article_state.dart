import 'package:ayobandung/models/all_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ArticleState extends Equatable {
  ArticleState([List props = const <dynamic>[]]) : super(props);
}

class InitialArticleState extends ArticleState {}

class InitialArticleLoading extends ArticleState {}

class InitialArticleLoaded extends ArticleState {
  final AllModel allModel;

  InitialArticleLoaded(this.allModel) : super([allModel]);
}
