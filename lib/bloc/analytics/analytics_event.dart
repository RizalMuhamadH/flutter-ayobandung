import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

@immutable
abstract class AnalyticsEvent extends Equatable {
  AnalyticsEvent([List props = const <dynamic>[]]) : super(props);
}

class SetAnalyticsCollectionEnabled extends AnalyticsEvent {
  final FirebaseAnalytics analytics;

  SetAnalyticsCollectionEnabled(this.analytics) : super([analytics]);
}

class LogViewItemList extends AnalyticsEvent {
  final FirebaseAnalytics analytics;
  final String category;

  LogViewItemList({this.analytics, this.category})
      : super([analytics, category]);
}

class LogSelectContent extends AnalyticsEvent {
  final FirebaseAnalytics analytics;
  final String id;
  final String type;

  LogSelectContent({this.analytics, this.id, this.type})
      : super([analytics, id, type]);
}

class LogSearch extends AnalyticsEvent {
  final FirebaseAnalytics analytics;
  final String keyword;
  final String date;

  LogSearch({this.analytics, this.keyword, this.date})
      : super([analytics, keyword, date]);
}

class AppOpen extends AnalyticsEvent {
  final FirebaseAnalytics analytics;

  AppOpen(this.analytics) : super([analytics]);
}

class SetCurrentScreen extends AnalyticsEvent {
  final FirebaseAnalytics analytics;

  SetCurrentScreen(this.analytics) : super([analytics]);
}
