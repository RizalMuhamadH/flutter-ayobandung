import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import './bloc.dart';

class AnalyticsBloc extends Bloc<AnalyticsEvent, AnalyticsState> {
  @override
  AnalyticsState get initialState => InitialAnalyticsState();

  @override
  Stream<AnalyticsState> mapEventToState(AnalyticsEvent event) async* {
    if (event is SetAnalyticsCollectionEnabled) {
      yield* await setAnalyticsCollectionEnabled(event.analytics);
    } else if(event is SetCurrentScreen){
      yield* await setCurrentScreen(event.analytics);
    } else if(event is AppOpen){
      yield* await appOpen(event.analytics);
    } else if(event is LogSearch){
      yield* await logSearch(event.analytics, event.keyword, event.date);
    } else if(event is LogSelectContent){
      yield* await logSelectContent(event.analytics, event.id, event.type);
    } else if (event is LogViewItemList){
      yield* await logViewItemList(event.analytics, event.category);
    }
  }

  Stream<InitialAnalyticsSetMessage> setAnalyticsCollectionEnabled(
      FirebaseAnalytics analytics) async* {
    await analytics.setAnalyticsCollectionEnabled(false);
    await analytics.setAnalyticsCollectionEnabled(true);

    yield InitialAnalyticsSetMessage('setAnalyticsCollectionEnabled succeeded');
  }

  Stream<InitialAnalyticsSetMessage> setCurrentScreen(
      FirebaseAnalytics analytics) async* {
    await analytics.setCurrentScreen(
      screenName: 'Analytics Home',
      screenClassOverride: 'AnalyticsHome',
    );
    yield InitialAnalyticsSetMessage('Current Screen succeeded');
  }

  Stream<InitialAnalyticsSetMessage> appOpen(
      FirebaseAnalytics analytics) async* {
    await analytics.logAppOpen();
    yield InitialAnalyticsSetMessage('App Open succeeded');
  }

  Stream<InitialAnalyticsSetMessage> logSearch(
      FirebaseAnalytics analytics, String keyword, String date) async* {
    await analytics.logSearch(
      searchTerm: keyword,
      startDate: date,
    );
    yield InitialAnalyticsSetMessage('Log Search succeeded');
  }

  Stream<InitialAnalyticsSetMessage> logSelectContent(
      FirebaseAnalytics analytics, String id, String type) async* {
    await analytics.logSelectContent(contentType: type, itemId: id);
    yield InitialAnalyticsSetMessage('Log Content succeeded');
  }

  Stream<InitialAnalyticsSetMessage> logViewItemList(
      FirebaseAnalytics analytics, String category) async* {
    await analytics.logViewItemList(
      itemCategory: category,
    );
    yield InitialAnalyticsSetMessage('Log Category succeeded');
  }
}

