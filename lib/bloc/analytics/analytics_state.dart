import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AnalyticsState extends Equatable{
  AnalyticsState([List props = const <dynamic>[]]) : super(props);
}

class InitialAnalyticsState extends AnalyticsState {}

class InitialAnalyticsSetMessage extends AnalyticsState {
  final String message;

  InitialAnalyticsSetMessage(this.message): super([message]);

}