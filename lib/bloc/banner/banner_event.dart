import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BannerEvent extends Equatable {
  BannerEvent([List props = const <dynamic>[]]) : super(props);
}

class GetBanner extends BannerEvent{}
