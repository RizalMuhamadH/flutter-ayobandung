import 'dart:async';
import 'dart:convert';
import 'package:ayobandung/models/banner.dart';
import 'package:ayobandung/utility/other.dart';
import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import './bloc.dart';

class BannerBloc extends Bloc<BannerEvent, BannerState> {
  @override
  BannerState get initialState => InitialBannerState();

  @override
  Stream<BannerState> mapEventToState(BannerEvent event) async* {
    if (event is GetBanner) {
      final all = await fetchGetBanner();
      yield InitialBannerLoaded(all);
    }
  }

  Future<Banner> fetchGetBanner() async {
    Response response = await Dio().get("${Other.BASE_URL}topBanner");
    var decodeJson = jsonDecode(response.toString());
    return Banner.fromJson(decodeJson);
  }

}
