import 'package:ayobandung/models/banner.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class BannerState extends Equatable {
  BannerState([List props = const <dynamic>[]]) : super(props);
}

class InitialBannerState extends BannerState {}


class InitialBannerLoaded extends BannerState {
  final Banner ads;

  InitialBannerLoaded(this.ads) : super([ads]);

}