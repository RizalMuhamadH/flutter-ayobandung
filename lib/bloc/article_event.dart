import 'package:ayobandung/models/all_model.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ArticleEvent extends Equatable {
  ArticleEvent([List props = const <dynamic>[]]) : super(props);
}

class getAll extends ArticleEvent {}
