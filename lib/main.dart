import 'package:ayobandung/bloc/analytics/analytics_bloc.dart';
import 'package:ayobandung/bloc/analytics/bloc.dart';
import 'package:ayobandung/bloc/banner/banner_bloc.dart';
import 'package:ayobandung/bloc/banner/bloc.dart';
import 'package:ayobandung/pages/about_us_page.dart';
import 'package:ayobandung/utility/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/foundation.dart';
import 'dart:async';
import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_version/get_version.dart';
import 'package:url_launcher/url_launcher.dart';
import './pages/init_article_page.dart';
import './pages/photo_gallery_page.dart';
import './pages/search_page.dart';
import './pages/video_gallery_page.dart';
import './pages/index_page.dart';
import './pages/content_page.dart';
import 'bloc/banner/banner_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseAnalyticsObserver observer =
      FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Ayo Bandung',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Color(0xFF077571),
//        primarySwatch: Colors.teal,
        fontFamily: 'Roboto',
      ),
      navigatorObservers: <NavigatorObserver>[observer],
      home: MyHomePage(
        title: 'Ayo Bandung',
        analytics: analytics,
        observer: observer,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.analytics, this.observer})
      : super(key: key);
  final String title;
  final FirebaseAnalytics analytics;
  final FirebaseAnalyticsObserver observer;

  @override
  _MyHomePageState createState() => _MyHomePageState(analytics, observer);
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  _MyHomePageState(this.analytics, this.observer);
  //analytics
  final FirebaseAnalyticsObserver observer;
  final FirebaseAnalytics analytics;
//  connectivity
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

//  snackbar
  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  int _selectedDrawerIndex = 0;

//  firebae message
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

//  bloc
  AnalyticsBloc _bloc = new AnalyticsBloc();
//  notification
  // FlutterLocalNotificationsPlugin _notificationsPlugin;

  bool _value = false;

  _openPageWidget(int page) {
    switch (page) {
      case 0:
        return InitArticlePage(analytics: analytics);
      case 1:
        return PhotoGalleryPage();
      case 2:
        return VideoGalleryPage();
      case 3:
        return IndexPage(
          analytics: analytics,
        );
      case 4:
        return AboutUsPage();
      default:
        return Center(
          child: Text("Error!"),
        );
    }
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  _onSelectItem(int index) {
    setState(() => _selectedDrawerIndex = index);
    Navigator.of(context).pop();
  }

  bool _onChangeSelected(int index) {
    return index == _selectedDrawerIndex ? true : false;
  }

  Widget _createDrawer() => Drawer(
        child: Container(
          decoration: BoxDecoration(color: Colors.white),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              DrawerHeader(
//              child: UserAccountsDrawerHeader(
//                accountName: Text("Movie App"),
//                accountEmail: Text("Inc."),
//              ),
                decoration: BoxDecoration(
                  color: Colors.teal,
                  image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('assets/image_header.png'),
                  ),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: _onChangeSelected(0)
                        ? Colors.black45.withOpacity(0.03)
                        : Colors.white),
                child: ListTile(
                  selected: _onChangeSelected(0),
                  title: Text("HOME"),
                  leading: Icon(Icons.home),
                  onTap: () {
                    _onSelectItem(0);
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: _onChangeSelected(1)
                        ? Colors.black45.withOpacity(0.03)
                        : Colors.white),
                child: ListTile(
                  selected: _onChangeSelected(1),
                  leading: Icon(Icons.photo_library),
                  title: Text(
                    "AYO FOTO",
                    style: TextStyle(fontSize: 14.0),
                  ),
                  onTap: () {
                    _onSelectItem(1);
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: _onChangeSelected(2)
                        ? Colors.black45.withOpacity(0.03)
                        : Colors.white),
                child: ListTile(
                  selected: _onChangeSelected(2),
                  leading: Icon(Icons.video_library),
                  title: Text(
                    "AYO VIDEO",
                    style: TextStyle(fontSize: 14.0),
                  ),
                  onTap: () {
                    _onSelectItem(2);
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: _onChangeSelected(3)
                        ? Colors.black45.withOpacity(0.03)
                        : Colors.white),
                child: ListTile(
                  selected: _onChangeSelected(3),
                  leading: Icon(Icons.art_track),
                  title: Text(
                    "INDEX",
                    style: TextStyle(fontSize: 14.0),
                  ),
                  onTap: () {
                    _onSelectItem(3);
                  },
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    color: _onChangeSelected(4)
                        ? Colors.black45.withOpacity(0.03)
                        : Colors.white),
                child: ListTile(
                  selected: _onChangeSelected(4),
                  leading: Icon(Icons.group),
                  title: Text(
                    "ABOUT US",
                    style: TextStyle(fontSize: 14.0),
                  ),
                  onTap: () {
                    _onSelectItem(4);
                  },
                ),
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: SwitchListTile(
                    value: _value,
                    title: Text(
                      "Notifikasi",
                      style: TextStyle(fontSize: 14.0),
                    ),
                    activeColor: Colors.blueGrey,
                    onChanged: (value) {
                      SharedPreference.setStateNotification(value);
                      if (value == true) {
                        _firebaseMessaging.subscribeToTopic("news");
                      } else {
                        _firebaseMessaging.unsubscribeFromTopic("news");
                      }
                      setState(() {
                        _value = value;
                      });
                    },
                  ),
                ),
              )
            ],
          ),
        ),
      );

  Future<void> _getAppInfo() async {
    String version;
    String osVersion;
    String versionCode;
    String appId;
    try {
      version = await GetVersion.projectVersion;
      osVersion = await GetVersion.platformVersion;
      versionCode = await GetVersion.projectCode;
      appId = await GetVersion.appID;
    } on PlatformException {
      print("Failed to get version");
    }
    print("$version $osVersion $versionCode $appId");

    if (osVersion.startsWith("Android")) {
      print(osVersion);
    }
//    https://play.google.com/store/apps/details?id=
//    PackageInfo info = await PackageInfo.fromPlatform();
//    print("Name : ${info.appName}, Package Name : ${info.packageName}, Version : ${info.version}, build Number : ${info.buildNumber}");
  }

  Future<void> _sendAnalyticsEvent() async {
    await analytics.logEvent(
      name: 'open_app',
      parameters: <String, dynamic>{
        'string': 'string',
        'int': 1,
        'bool': true,
      },
    );
    print('open app');
  }

  @override
  void initState() {
    // TODO: implement initState
//    packageInfoMock();
//    _bloc = BlocProvider.of<BannerBloc>(context);
    _bloc.dispatch(SetAnalyticsCollectionEnabled(analytics));
    _bloc.dispatch(AppOpen(analytics));
    _bloc.dispatch(SetCurrentScreen(analytics));
    _sendAnalyticsEvent();

    //notif
    // _notificationsPlugin = FlutterLocalNotificationsPlugin();
    // var android = AndroidInitializationSettings("ic_stat_ic_notification");
    // var ios = IOSInitializationSettings();
    // var initSettings = InitializationSettings(android, ios);

    // _notificationsPlugin.initialize(initSettings,onSelectNotification: onSelectNotification);

    _firebaseMessaging.configure(
      onLaunch: (Map<String, dynamic> msg) async {
        print("Launch : $msg");
        if (Platform.isIOS) {
          onSelectNotification(msg["id"]);
        } else {
          onSelectNotification(msg["data"]["id"]);
        }
//        showNotification(1, msg["notification"]["title"], msg["notification"]["body"], msg["data"]["id"]);
      },
      onResume: (Map<String, dynamic> msg) async {
        print("Resume : $msg");
        if (Platform.isIOS) {
          onSelectNotification(msg["id"]);
        } else {
          onSelectNotification(msg["data"]["id"]);
        }
//        showNotification(1, msg["notification"]["title"], msg["notification"]["body"], msg["data"]["id"]);
      },
      onMessage: (Map<String, dynamic> msg) async {
        print("Message : $msg");
        if (Platform.isIOS) {
          onSelectNotification(msg["id"]);
        } else {
//          onSelectNotification(msg["data"]["id"]);
        }
        // if(Platform.isIOS){
        // showNotification(1, msg["aps"]["alert"]["title"], msg["aps"]["alert"]["body"], msg["id"]);
        // } else {
        // showNotification(1, msg["notification"]["title"], msg["notification"]["body"], msg["data"]["id"]);
        // }
        // showNotification(1, msg["notification"]["title"], msg["notification"]["body"], msg["data"]["id"]);
      },
    );
    _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Setting registered: $settings");
    });
    _firebaseMessaging.getToken().then((token) {
      print("Token : $token");
    });
    SharedPreference.getStateNotification().then((val) {
      print("status $val");
      if (val == null) {
        SharedPreference.setStateNotification(true);
        _firebaseMessaging.subscribeToTopic("news");
        _value = true;
        print("subscribe");
      } else {
        setState(() {
          _value = val;
        });
      }
    });
    _firebaseMessaging.setAutoInitEnabled(false);
    _getAppInfo();

    SharedPreference.getStateNotification().then((val) {
      if (val != null) {
        setState(() {
          _value = val;
        });
      }
    });

    super.initState();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  // showNotification(int id, String title, String body, String payload) async {
  //   var android =
  //       AndroidNotificationDetails("999", "channel_id", "description");
  //   var ios = IOSNotificationDetails();
  //   var platform = NotificationDetails(android, ios);
  //   await _notificationsPlugin.show(id, title, body, platform,
  //       payload: payload);
  // }

  Future onSelectNotification(String payload) async {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (BuildContext context) => ContentPage(
          id: payload,
          tag: "notify_$payload",
        ),
      ),
    );
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _connectivitySubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        bottom: topBanner(),
        elevation: 0.0,
        title: Image.asset(
          'assets/ayobandung.png',
          fit: BoxFit.cover,
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                  context: context, delegate: SearchPage(analytics: analytics));
            },
          ),
        ],
      ),
      drawer: _createDrawer(),
//      body: TestPage(),
      body: _openPageWidget(_selectedDrawerIndex),
    );
  }

  Widget topBanner() => PreferredSize(
        child: BlocProvider<BannerBloc>(
          builder: (context) => BannerBloc()..dispatch(GetBanner()),
          child:
              BlocBuilder<BannerBloc, BannerState>(condition: (prev, current) {
            // ignore: missing_return
            print("CONDISION $prev $current");
            return null;
          },
//              bloc: _bloc,
                  // ignore: missing_return
                  builder: (BuildContext context, BannerState state) {
            if (state is InitialBannerState) {
              return Container();
            } else if (state is InitialBannerLoaded) {
              return Expanded(
                child: GestureDetector(
                  onTap: () async {
                    if (await canLaunch(state.ads.adsLink1) != null) {
                      await launch(state.ads.adsLink1);
                    } else {
                      throw "Could not launch $state.ads.adsLink1";
                    }
                  },
                  child: _cacheNetworkImage(state.ads.topBanner, BoxFit.fill,
                      40.0, MediaQuery.of(context).size.width),
                ),
              );
            }
          }),
        ),
        preferredSize: Size.fromHeight(40.0),
      );

  Widget bannerFuture() => FutureBuilder(
      future: BannerBloc().fetchGetBanner(),
      builder: (context, snapshot) {
        return Container();
      });
}
