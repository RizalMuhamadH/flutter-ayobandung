import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import '../models/article_model.dart';

class NewsCategoryScoped extends Model {
  Article _article;

  Article get article => _article;

  String _category = "0";

  String get category => _category;

  Future<Article> fetchgetArticle(String cat, int pageRecent) async {
    _category = cat;

    Response response = await Dio().post(
      "https://www.ayobandung.com/api_mob_new/getRecentArticle",
      data: FormData.fromMap({"cat_id": cat, "limit": 20, "page": pageRecent}),
    );

    var decodeJson = jsonDecode(response.toString());

    if (identical(Article.fromJson(decodeJson).kode, 200)) {
      article == null
          ? _article = Article.fromJson(decodeJson)
          : article.data.addAll(Article.fromJson(decodeJson).data);
    }

    return article;
  }
}
