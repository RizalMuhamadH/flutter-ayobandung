import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class FancyButton extends StatelessWidget {
  final GestureTapCallback onPressed;
  final bool selected;
  final String title;

  FancyButton({@required this.onPressed, this.selected, this.title});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      child: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 20.0,
        ),
        child: Text(
          title,
          style: TextStyle(color: Colors.white, fontFamily: 'Roboto', fontWeight: selected ? FontWeight.bold : FontWeight.normal),
        ),
      ),
      fillColor: selected ? Colors.blue[800] : Colors.blueAccent,
      splashColor: Colors.blue[800],
      onPressed: onPressed,
      shape: const StadiumBorder(),
    );
  }
}
