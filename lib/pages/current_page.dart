import 'package:ayobandung/models/article.dart' as model;

import '../models/article_model.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import '../utility/other.dart';
import 'package:intl/intl.dart';
import './headline_slide.dart';
import '../models/all_model.dart';
import 'package:page_indicator/page_indicator.dart';

class CurrentPage extends StatefulWidget {
  @override
  _CurrentPageState createState() => _CurrentPageState();
}

class _CurrentPageState extends State<CurrentPage> {
  final PageController _controller = PageController();

  AllModel allModel;
  Article article;
  Article headline;
  int _pageRecent = 1;
  int _totalRecent = 0;
  int _totalHeadline = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchGetHeadlineNews();
    _fetchGetRecentNews();
    _fetchGetAllNews();
  }

  _fetchGetAllNews() async {
    Response response = await Dio().get("${Other.BASE_URL}getAll");
    var decodeJson = jsonDecode(response.toString());

    allModel = AllModel.fromJson(decodeJson);

    print(allModel.recent.toString());

    setState(() {
      _totalRecent = allModel.recent.length;
      print("$_totalRecent");
    });
  }

  _fetchGetRecentNews() async {
    Response response = await Dio().post(
      "https://www.ayobandung.com/api_mob_new/getRecentArticle",
      data: FormData.fromMap({"cat_id": "0", "limit": 20, "page": _pageRecent}),
    );
    var decodeJson = jsonDecode(response.toString());

    article == null
        ? article = Article.fromJson(decodeJson)
        : article.data.addAll(Article.fromJson(decodeJson).data);

    setState(() {
      _totalRecent = article.data.length;
    });
  }

  _fetchGetHeadlineNews() async {
    Response response = await Dio().post(
      "https://www.ayobandung.com/api_mob_new/getHeadline",
      data: FormData.fromMap({"cat_id": "0", "limit": 5}),
    );
    var decodeJson = jsonDecode(response.toString());

    headline = Article.fromJson(decodeJson);
    setState(() {
      _totalHeadline = headline.data.length;
    });
  }

  Widget _createListView(AllModel article) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: article.recent.length,
        itemBuilder: (BuildContext context, int index) {
          if (index >= article.recent.length - 1) {
            _pageRecent++;
            _fetchGetRecentNews();
          }
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Card(
              child: Hero(
                  tag: index, child: _buildListItem(article.recent[index])),
              elevation: 4.0,
            ),
          );
        });
  }

  Widget _buildHeadlineView() {
    return PageIndicatorContainer(
      align: IndicatorAlign.bottom,
      length: _totalHeadline,
      indicatorColor: Colors.white,
      indicatorSelectorColor: Colors.red,
      size: 5.0,
      indicatorSpace: 5.0,
      pageView: PageView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _totalHeadline,
          itemBuilder: (BuildContext context, int index) {
//            return HeadlineSlide(headline.data[index], null, "headline");
          }),
    );
  }

  Widget _buildListItem(model.Article item) => Material(
        child: Container(
          height: 100.0,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(6.0),
                child: Container(
                  width: 100.0,
                  height: 100.0,
                  child: Image.network(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDate))}/${DateFormat('MM').format(DateTime.parse(item.postDate))}/${DateFormat('dd').format(DateTime.parse(item.postDate))}/${item.postId}/${item.postImageContent}",
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0),
                        child: Text(
                          item.postTitle,
                          style: TextStyle(
                              fontSize: 14.0, fontWeight: FontWeight.bold),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              automaticallyImplyLeading: false,
              expandedHeight: 290.0,
              floating: false,
              backgroundColor: Colors.white,
              flexibleSpace: FlexibleSpaceBar(
                background: Stack(
                  children: <Widget>[
                    _buildHeadlineView(),
//                  HeadlineSlide(),
                  ],
                ),
              ),
            ),
          ];
        },
        body: _createListView(allModel),
      ),
    );
  }
}
