import 'package:ayobandung/models/about_model.dart';
import 'package:ayobandung/pages/details_about_page.dart';
/**
 * Created by Rizal Muhamad H on 7/16/2019 1:12 PM.
 * rizalmuhamadh@gmail.com
 */
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:dio/dio.dart';
import 'dart:convert';

import 'package:flutter/services.dart';

class AboutUsPage extends StatefulWidget {
  @override
  _AboutUsPageState createState() => _AboutUsPageState();
}

class _AboutUsPageState extends State<AboutUsPage> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  AboutModel _aboutModel;

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
//      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  Future<AboutModel> _fetchGetAboutUs() async {
    Response response = await Dio()
        .get("https://www.ayobandung.com/api_mob_new/getPages");

    var decodeJson = jsonDecode(response.toString());
//    debugPrint("fetch $decodeJson");
    if (identical(AboutModel.fromJson(decodeJson).kode, 200)) {
      _aboutModel = AboutModel.fromJson(decodeJson);
    }

    debugPrint(_aboutModel.data[2].content);

    return _aboutModel;
  }

  Widget _futureBuilder() {
    return FutureBuilder(
        future: _fetchGetAboutUs(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return _aboutModel == null
                  ? Container(
                      child: Center(
                        child: Text(
                          "data tidak ditemukan",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : _buildListView(snapshot.data, context);
            default:
              if (snapshot.hasError)
                return Center(child: Text("Error : ${snapshot.error}"));
          }
        });
  }

  @override
  void initState() {
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
    super.initState();
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  Widget _buildListView(AboutModel data, BuildContext context) => ListView(
        children: data.data.map<Widget>((item) => _buildItem(item, context)).toList(),
      );

  _buildItem(Data item, BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => DetailsAboutPage(title: item.title,text: item.content)));
      },
      child: SizedBox(
        height: 250.0,
        child: Stack(
          children: <Widget>[
            Center(
              child: _cacheNetworkImage(
                  item.img,
                  BoxFit.fill,
                  MediaQuery.of(context).size.height,
                  MediaQuery.of(context).size.width),
            ),
            Center(
              child: Text(
                item.title,
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    // TODO: Implement build
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        child: _status == true
            ? _aboutModel == null
                ? _futureBuilder()
                : _buildListView(_aboutModel, context)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
