import 'package:admob_flutter/admob_flutter.dart';
import 'package:ayobandung/models/user_netizen_model.dart';
import 'package:ayobandung/utility/adsense.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import '../models/article_detail_model.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import '../utility/other.dart';
import 'package:url_launcher/url_launcher.dart' as direct;
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import '../utility/other.dart';
import 'package:html/dom.dart' as dom;

class ContentPage extends StatefulWidget {
  final String id;
  final String tag;
  String image;
  String category;
  final FirebaseAnalytics analytics;

  ContentPage({this.id, this.tag, this.image, this.category, this.analytics});

  @override
  _ContentPageState createState() => _ContentPageState();
}

enum TtsState { playing, stopped }

class _ContentPageState extends State<ContentPage>
    with TickerProviderStateMixin {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  ArticleDetails _details;
  UserNetizen _netizen;

  String _image;
  String _category;
  bool _collapse = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    _controller = AnimationController(
        duration: const Duration(milliseconds: 1000), vsync: this);
    _animation = CurvedAnimation(parent: _controller, curve: Curves.easeIn);

    /*animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        controller.forward();
      }
    });*/
    _fetchGetNetizen();

    _controller.forward();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  Future<ArticleDetails> _fetchGetArticle() async {
    print("id " + widget.id);
    Response response = await Dio().post(
      "${Other.BASE_URL}getArticle",
      data: FormData.fromMap({"id": widget.id}),
    );
    var decodeJson = jsonDecode(response.toString());
    if (identical(ArticleDetails.fromJson(decodeJson).kode, 200)) {
      _details = ArticleDetails.fromJson(decodeJson);

      print(_details.newsCategory.data.length);
      if (widget.image == null) {
        setState(() {
          widget.image =
              "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].postImageContent}";
          widget.category = _details.result[0].categoryName;
        });
      }
    }

    return _details;
  }

  Future<UserNetizen> _fetchGetNetizen() async {
    Response response =
        await Dio().get("${Other.BASE_URL}getNetizen/" + widget.id);
    var rest = jsonDecode(response.toString());
    if (rest["kode"] != 404) {
      _netizen = UserNetizen.fromJson(rest["user"]);
      print(_netizen.netNama);
    }
    print("Netizen ${rest}");
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget _createHtmlView(BuildContext context) {
    return Html(
      data: "<p align=\"justify\">${_details.result[0].postContent}</p>",
      padding: const EdgeInsets.only(
        left: 5.0,
      ),
      onLinkTap: (url) {
        String _url = url.substring(0, url.lastIndexOf("/")) + "";
        String id = _url.substring(_url.lastIndexOf("/") + 1);
        if (url.startsWith("https://www.ayobandung.com/read/")) {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) => ContentPage(
                id: id,
                tag: "category_$id",
              ),
            ),
          );
        } else {
          _launchURL(url);
        }
      },
//      customRender: (node, children) {
//        print(node.attributes['href']);
//        print(children);
//        print(node.parent);
//        if (node is dom.Element) {
//          print(node.localName);
//          print(node.text);
//
//          switch (node.localName) {
//            case "p":
//              return Column(crossAxisAlignment: CrossAxisAlignment.stretch,children: children,);
//          }
//        }
//      },
      useRichText: true,
//            renderNewlines: true,
      defaultTextStyle: TextStyle(fontSize: 16, color: Colors.black),
    );
  }

  Widget _futureBuilderContent(BuildContext context) => FutureBuilder(
      future: _fetchGetArticle(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _details == null
                ? Container(
                    child: Center(
                      child: Text(
                        "data tidak ditemukan",
                        textAlign: TextAlign.center,
                      ),
                    ),
                  )
                : _createListContent(context);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
        return Container();
      });

  Widget _buildSliverBar() => SliverAppBar(
        expandedHeight: 250.0,
        floating: false,
        pinned: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Share.share(
                  "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${_details.result[0].slug}");
            },
          ),
        ],
        flexibleSpace: FlexibleSpaceBar(
          centerTitle: false,
          title: _collapse
              ? FadeTransition(
                  opacity: _animation,
                  child: Text(
                    widget.category == null
                        ? ""
                        : widget.category.toUpperCase(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold),
                  ),
                )
              : Container(),
          background: Hero(
            tag: widget.tag,
            child: widget.image == null
                ? CircularProgressIndicator()
                : _cacheNetworkImage(
                    widget.image,
                    BoxFit.fill,
                    MediaQuery.of(context).size.height,
                    MediaQuery.of(context).size.width),
          ),
        ),
      );

  Widget _buildContentView(BuildContext context) => NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          _collapse = innerBoxIsScrolled;
          return <Widget>[
            _buildSliverBar(),
          ];
        },
        body: _status == true
            ? _futureBuilderContent(context)
            : Container(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(Icons.signal_wifi_off),
                      Text(
                        "Connection Failed",
                        style: TextStyle(fontFamily: 'Montserrat'),
                      ),
                    ],
                  ),
                ),
              ),
      );

  _createListViewRelated(Related article, BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: article.data.length,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
//            if (index >= _totalRecent - 1) {
//              print(article.data[index].postTitle);
//              _pageRecent++;
//              _fetchGetRecentNews();
//            }

          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ContentPage(
                              id: article.data[index].postId,
                              tag: "category_$index",
                              category: article.data[index].categoryName
                                  .toUpperCase(),
                              image:
                                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                            )));
                  },
                  child: Hero(
                      tag: "category_$index",
                      child: _buildListItem(article.data[index], context)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  _createListViewMust(NewsCategory article, BuildContext context) {
    return ListView(
      physics: const NeverScrollableScrollPhysics(),
      children: article.data
          .map((item) => Padding(
                padding: EdgeInsets.only(left: 2.0, right: 2.0),
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => ContentPage(
                                  id: item.postId,
                                  tag: "category_${item.postId}",
                                  category: item.categoryName.toUpperCase(),
                                  image:
                                      "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                                )));
                      },
                      child: Hero(
                          tag: "category_${item.postId}",
                          child: _buildListItem(item, context)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Divider(
                        height: 1.0,
                        color: Colors.black26,
                      ),
                    ),
                  ],
                ),
              ))
          .toList(),
    );
//    return ListView.builder(
//        scrollDirection: Axis.vertical,
//        itemCount: article.data.length,
//        physics: const NeverScrollableScrollPhysics(),
//        itemBuilder: (BuildContext context, int index) {
//
//          return Padding(
//            padding: EdgeInsets.only(left: 2.0, right: 2.0),
//            child: Column(
//              children: <Widget>[
//                InkWell(
//                  onTap: () {
//                    Navigator.of(context).push(MaterialPageRoute(
//                        builder: (BuildContext context) => ContentPage(
//                          id: article.data[index].postId,
//                          tag: "category_$index",
//                          category: article.data[index].categoryName,
//                          image:
//                          "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
//                        )));
//                  },
//                  child: Hero(
//                      tag: "category_$index",
//                      child: _buildListItem(article.data[index], context)),
//                ),
//                Padding(
//                  padding: const EdgeInsets.only(top: 5.0),
//                  child: Divider(
//                    height: 1.0,
//                    color: Colors.black26,
//                  ),
//                ),
//              ],
//            ),
//          );
//        });
  }

  Widget _buildListItem(Data item, BuildContext context) => Material(
        child: Container(
          height: 100.0,
          child: Padding(
            padding: const EdgeInsets.only(
              top: 5.0,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(6.0),
                  child: Container(
                    height: 100.0,
                    width: 100.0,
                    child: _cacheNetworkImage(
                        "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                        BoxFit.fill,
                        100.0,
                        100.0),
                  ),
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(right: 5.0),
                              child: Icon(
                                Icons.category,
                                size: 11.0,
                                color: Colors.blueAccent,
                              ),
                            ),
                            Text(
                              item.categoryName.toUpperCase(),
                              style: TextStyle(
                                fontSize: 11.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Padding(
                          padding: const EdgeInsets.only(right: 1.0),
                          child: SizedBox(
                            height: 38.0,
                            child: Text(
                              item.postTitle,
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              item.editor == null ||
                                      item.editor == "Suara.com" ||
                                      item.editor == "Republika.co.id"
                                  ? TextSpan()
                                  : TextSpan(children: <TextSpan>[
                                      TextSpan(
                                        text: "Oleh ",
                                        style: TextStyle(
                                          fontSize: 11.0,
                                        ),
                                      ),
                                      TextSpan(
                                          text: "${item.editor.toUpperCase()} ",
                                          style: TextStyle(
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.bold,
                                          )),
                                    ]),
                              TextSpan(
                                text: _getDateBetween(item.postDate),
                                style: TextStyle(
                                  fontSize: 11.0,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 8.0, right: 3.0),
                    child: InkWell(
                      onTap: () {
                        Share.share(
                            "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.slug}");
                      },
                      child: Icon(
                        Icons.share,
                        size: 21.0,
                        color: Colors.black54,
                      ),
                    )),
              ],
            ),
          ),
        ),
      );

  Widget userNetizen(UserNetizen user) => Material(
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(
            top: 5.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(6.0),
                  child: SizedBox(
                    height: 100.0,
                    width: 100.0,
                    child: CachedNetworkImage(
                      imageUrl:
                          "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${user.netPhoto}",
                      placeholder: (context, url) =>
                          Image.asset('assets/loading_image.png'),
                      errorWidget: (context, url, error) =>
                          Image.asset('assets/person_icon.png'),
                      fadeInDuration: Duration(seconds: 2),
                      fadeOutDuration: Duration(seconds: 1),
                      height: 100.0,
                      width: 100.0,
                      fit: BoxFit.cover,
                    ),
//              _cacheNetworkImage(
//                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(_details.result[0].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(_details.result[0].postDateCreated))}/${_details.result[0].postId}/${user.netPhoto}",
//                  BoxFit.fill,
//                  100.0,
//                  100.0),
                  )),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            user.netNama,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10.0, right: 2.0),
                      child: Text(
                        user.netDeskripsi,
                        style: TextStyle(
                          fontSize: 11.0,
                          fontWeight: FontWeight.bold,
                        ),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 2),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  _createListContent(BuildContext context) {
    return ListView(
      scrollDirection: Axis.vertical,
      children: <Widget>[
        _details.result[0].postImageCaption == null
            ? Container()
            : Container(
                margin: EdgeInsets.only(bottom: 15.0),
                child: Text(
                  _details.result[0].postImageCaption,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 11.0),
                )),
        Container(
          margin: EdgeInsets.only(top: 6.0, bottom: 6.0),
          child: AdmobBanner(
            adUnitId: Adsense.BANNER_ID_1,
            adSize: AdmobBannerSize.BANNER,
            listener: (AdmobAdEvent event, Map<String, dynamic> args) {
              print('TEST $event $args');
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: const EdgeInsets.only(right: 5.0),
                child: Icon(
                  Icons.category,
                  size: 11.0,
                  color: Colors.blueAccent,
                ),
              ),
              Text(
                _details.result[0].categoryName.toUpperCase(),
                style: TextStyle(
                  fontSize: 11.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 16.0, left: 5.0),
          child: Text(
            _details.result[0].postTitle,
            style: TextStyle(
                fontSize: 22.0,
                color: Colors.black,
                fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 5.0, bottom: 10.0),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                _details.result[0].reporter == null ||
                        _details.result[0].reporter == "Suara.com" ||
                        _details.result[0].reporter == "Republika.co.id"
                    ? TextSpan()
                    : TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: "Oleh ",
                          style: TextStyle(
                            fontSize: 11.0,
                          ),
                        ),
                        TextSpan(
                            text:
                                "${_details.result[0].reporter.toUpperCase()} -",
                            style: TextStyle(
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                            )),
                      ]),
                TextSpan(
                  text:
                      " ${DateFormat.yMMMMEEEEd("id").format(DateTime.parse(_details.result[0].postDate))} ${DateFormat("Hms").format(DateTime.parse(_details.result[0].postDate))} WIB",
                  style: TextStyle(
                    fontSize: 11.0,
                  ),
                ),
              ],
            ),
          ),
        ),
        Divider(
          height: 3.0,
          color: Colors.black45,
        ),
        Container(
          margin: EdgeInsets.only(top: 15.0),
          height: 40.0,
          child: _cacheNetworkImage(
              _details.top,
              BoxFit.fitWidth,
              MediaQuery.of(context).size.height,
              MediaQuery.of(context).size.width),
        ),
        _createHtmlView(context),
        _netizen != null ? userNetizen(_netizen) : Container(),
        _netizen != null
            ? Container(
                padding: EdgeInsets.symmetric(vertical: 20.0),
                margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                decoration: BoxDecoration(color: Colors.amber.withOpacity(0.5)),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    style: TextStyle(color: Colors.black, fontSize: 14.0),
                    children: <TextSpan>[
                      TextSpan(
                          text:
                              "Tulisan ini adalah kiriman netizen, isi tulisan di luar tanggung jawab redaksi.\n"),
                      TextSpan(
                          text: "Ayo Menulis Klik di sini",
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              await direct.launch(
                                  "http://ayobandung.com/ayo-netizen",
                                  forceSafariVC: false);
                            },
                          style: Theme.of(context)
                              .textTheme
                              .title
                              .copyWith(color: Theme.of(context).accentColor)),
                    ],
                  ),
                ),
              )
            : Container(),
        _details.result[0].reporter == "Suara.com" ||
                _details.result[0].reporter == "Republika.co.id"
            ? Container(
                padding: EdgeInsets.symmetric(vertical: 20.0),
                margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                decoration: BoxDecoration(color: Colors.amber.withOpacity(0.5)),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    style: TextStyle(color: Colors.black, fontSize: 14.0),
                    children: <TextSpan>[
                      TextSpan(
                          text:
                              "Berita ini merupakan hasil kerjasama Ayo Media Network dan "),
                      TextSpan(
                          text: _details.result[0].reporter,
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              String url = "";
                              if (_details.result[0].reporter == "Suara.com") {
                                url = "https://www.suara.com/";
                              } else if (_details.result[0].reporter ==
                                  "Republika.co.id") {
                                url = "https://www.republika.co.id/";
                              }
                              await direct.launch(url, forceSafariVC: false);
                            },
                          style: Theme.of(context)
                              .textTheme
                              .body2
                              .copyWith(color: Theme.of(context).accentColor)),
                      TextSpan(text: "."),
                      TextSpan(
                        text:
                            "\nIsi tulisan ini di luar tanggung jawab Ayo Media Network.",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
              )
            : Container(),
        Padding(
          padding: const EdgeInsets.only(left: 5.0, bottom: 10.0),
          child: RichText(
            text: TextSpan(
              style: DefaultTextStyle.of(context).style,
              children: <TextSpan>[
                TextSpan(children: <TextSpan>[
                  TextSpan(
                    text: "Editor :  ",
                    style: TextStyle(
                      fontSize: 11.0,
                    ),
                  ),
                  TextSpan(
                      text: "${_details.result[0].editor.toUpperCase()} ",
                      style: TextStyle(
                        fontSize: 11.0,
                        fontWeight: FontWeight.bold,
                      )),
                ]),
              ],
            ),
          ),
        ),
        Container(
          height: 40.0,
          child: _cacheNetworkImage(
              _details.bottom,
              BoxFit.fitWidth,
              MediaQuery.of(context).size.height,
              MediaQuery.of(context).size.width),
        ),
        identical(_details.related.kode, 502) == true
            ? Container()
            : _createTitle("Berita Terkait".toUpperCase()),
        identical(_details.related.kode, 502) == true
            ? Container()
            : Container(
                height: (_details.related.data.length * 106).toDouble(),
                child: _createListViewRelated(_details.related, context),
              ),
        identical(_details.newsCategory.kode, 502) == true
            ? Container()
            : _createTitle(widget.category.toUpperCase()),
        identical(_details.newsCategory.kode, 502) == true
            ? Container()
            : Container(
                height: (_details.newsCategory.data.length * 106).toDouble(),
                child: _createListViewMust(_details.newsCategory, context),
              ),
      ],
    );
  }

  Widget _createTitle(String title) {
    return Container(
      margin: EdgeInsets.only(top: 25.0, bottom: 5.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
//          Divider(height: 3.0, color: Colors.black45,),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0, left: 10.0),
            child: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 14.0),
            ),
          ),
          Divider(
            height: 3.0,
            color: Colors.black45,
          ),
        ],
      ),
    );
  }

  _launchURL(String url) async {
    if (await direct.canLaunch(url)) {
      await direct.launch(url);
    } else {
      throw "Could not launch $url";
    }
  }

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();
    Admob.initialize(Adsense.APP_ID);

    return Scaffold(
      key: _scaffoldKey,
      body: _buildContentView(context),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
