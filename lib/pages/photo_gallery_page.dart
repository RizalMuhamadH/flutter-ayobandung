import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import '../utility/other.dart';
import '../models/photo_gallery_model.dart';
import '../pages/photo_page.dart';

class PhotoGalleryPage extends StatefulWidget {
  @override
  _PhotoGalleryPageState createState() => _PhotoGalleryPageState();
}

class _PhotoGalleryPageState extends State<PhotoGalleryPage> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  GalleryPhotoModel _gallery;
  int _totalPhoto = 0;
  int _pagePhoto = 0;

  Future<GalleryPhotoModel> _fetchGetPhotoGallery() async {
    Response response = await Dio().post(
      "${Other.BASE_URL}getRecentPhoto",
      data: FormData.fromMap({"page": _pagePhoto, "limit": 20}),
    );
    var decodeJson = jsonDecode(response.toString());

    print(GalleryPhotoModel.fromJson(decodeJson).kode);
    if (identical(GalleryPhotoModel.fromJson(decodeJson).kode, 200)) {
      _gallery == null
          ? _gallery = GalleryPhotoModel.fromJson(decodeJson)
          : _gallery.data.addAll(GalleryPhotoModel.fromJson(decodeJson).data);

      setState(() {
        _totalPhoto = _gallery.data.length;
      });
    }

    return _gallery;
  }

//  Widget _createGridView(GalleryPhotoModel gallery) => GridView.builder(
//      itemCount: _totalPhoto,
//      gridDelegate:
//          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
//      itemBuilder: (BuildContext context, int index) {
//        if (_status) {
//          if (index >= _totalPhoto - 1) {
//            _pagePhoto++;
//            _fetchGetPhotoGallery();
//          }
//        } else {
//          final snackBar = SnackBar(content: Text("Koneksi internet terputus"));
//          Scaffold.of(context).showSnackBar(snackBar);
//        }
//
//        return _createFotoItem(gallery.data[index], index);
//      });

  Widget _straggerGridView(GalleryPhotoModel gallery) =>
      StaggeredGridView.countBuilder(
        crossAxisCount: 4,
        itemCount: _totalPhoto,
        itemBuilder: (BuildContext context, int index) {
          if (_status) {
            if (index >= _totalPhoto - 1) {
              _pagePhoto++;
              _fetchGetPhotoGallery();
            }
          } else {
            final snackBar =
                SnackBar(content: Text("Koneksi internet terputus"));
            Scaffold.of(context).showSnackBar(snackBar);
          }

          return _createFotoItem(gallery.data[index], index);
        },
        staggeredTileBuilder: (int index) =>
            StaggeredTile.count(2, index.isEven ? 2 : 3),
//        mainAxisSpacing: 2.0,
//        crossAxisSpacing: 2.0,
      );

  Widget _createFotoItem(Data item, index) {
    return Material(
      elevation: 15.0,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => PhotoPage(item.postId)));
        },
        child: Stack(
          children: <Widget>[
            Hero(
              tag: index,
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                    minWidth: double.infinity, minHeight: double.infinity),
                child: _cacheNetworkImage(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                    MediaQuery.of(context).size.height,
                    MediaQuery.of(context).size.width),
              ),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: SizedBox(
                  height: 36.0,
                  width: double.infinity,
                  child: Container(
                    decoration:
                        BoxDecoration(color: Colors.black.withOpacity(0.8)),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
                      child: Text(
                        item.postTitle,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget _cacheNetworkImage(String imageUrl, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: BoxFit.fill,
      );

  Widget _buildViewShow() => FutureBuilder(
      future: _fetchGetPhotoGallery(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _straggerGridView(_gallery);
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: _status == true
          ? _gallery == null ? _buildViewShow() : _straggerGridView(_gallery)
          : Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.signal_wifi_off),
                    Text(
                      "Connection Failed",
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
//      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
