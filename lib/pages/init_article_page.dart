import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import './article_page.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import '../utility/other.dart';
import '../models/category_model.dart';
import '../button/fancy_button.dart';
import './news_category_page.dart';
import './dynamic_article_page.dart';

class InitArticlePage extends StatefulWidget {
  final FirebaseAnalytics analytics;

  const InitArticlePage({Key key, this.analytics}) : super(key: key);
  @override
  _InitArticlePageState createState() => _InitArticlePageState();
}

class _InitArticlePageState extends State<InitArticlePage>
    with SingleTickerProviderStateMixin {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  List<Tab> tabs = List();
  TabController _tabController;

  CategoryModel _category;
  int _totalCategory = 0;
  String _cat = "0";

  _fetchGetCategory() async {
    Response response = await Dio().get("${Other.BASE_URL}getKanal");
    var decodeJson = jsonDecode(response.toString());

    _category = CategoryModel.fromJson(decodeJson);
    _totalCategory = _category.category.length;

    _tabController = new TabController(vsync: this, length: _totalCategory);

    for (int i = 0; i < _category.category.length; i++) {
      tabs.add(Tab(
        text: _category.category[i].categoryId,
      ));
    }

    return _category;
  }

  Widget _buildView() => Stack(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: 45.0),
            child: TabBarView(
              controller: _tabController,
              children: _category.category.map((item) {
                return DynamicArticlePage(
                  page: int.parse(item.categoryId),
                  category: item.categoryId,
                  analytics: widget.analytics,
                  nameCategory: item.categoryName
                );
              }).toList(),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              decoration: BoxDecoration(color: Colors.white.withOpacity(0.8)),
              height: 45.0,
              child: _buildTabBarCategory(),
            ),
          ),
        ],
      );

  Widget _buildTabBarCategory() => TabBar(
        isScrollable: true,
        unselectedLabelColor: Colors.grey,
        labelColor: Colors.white,
        labelStyle: TextStyle(fontWeight: FontWeight.bold),
        indicatorSize: TabBarIndicatorSize.tab,
        indicator: new BubbleTabIndicator(
          indicatorHeight: 25.0,
          indicatorColor: Colors.blueAccent,
          tabBarIndicatorSize: TabBarIndicatorSize.tab,
        ),
        tabs: _category.category
            .map((item) => Tab(
                  text: item.categoryName,
                ))
            .toList(),
        controller: _tabController,
      );

  Widget _futureBuilderView() => FutureBuilder(
      future: _fetchGetCategory(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _category == null ? Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.signal_wifi_off),
                    Text(
                      "Connection Failed",
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                  ],
                ),
              ),
            ) : _buildView();
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
      });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: _status == true
          ? _category == null ? _futureBuilderView() : _buildView()
          : Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.signal_wifi_off),
                    Text(
                      "Connection Failed",
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
//      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
    _connectivitySubscription.cancel();
    super.dispose();
  }
}
