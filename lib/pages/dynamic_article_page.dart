import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import './article_page.dart';
import './news_category_page.dart';

// ignore: must_be_immutable
class DynamicArticlePage extends StatelessWidget {
  int page;
  String category;
  FirebaseAnalytics analytics;
  String nameCategory;

  DynamicArticlePage({this.page, this.category, this.analytics, this.nameCategory});
  @override
  Widget build(BuildContext context) {
    return page == 0 ? ArticlePage(analytics: analytics) : NewsCategoryPage(category: category, analytics: analytics, name: nameCategory);
  }
}
