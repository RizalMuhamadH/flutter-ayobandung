import 'package:ayobandung/models/object/Persib.dart';
import 'package:ayobandung/models/object/headline.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../models/article_model.dart';
import '../utility/other.dart';

class HeadlineSlide extends StatefulWidget {
  final Headline headline;
  final Persib persib;
  final String tag;

  HeadlineSlide({this.headline, this.persib, this.tag});

  @override
  _HeadlineSlideState createState() => _HeadlineSlideState();
}

Widget _cacheNetworkImage(String imageUrl, double height, double width) =>
    CachedNetworkImage(
      imageUrl: imageUrl,
      placeholder: (context, url) => Image.asset('assets/loading_image.png'),
      errorWidget: (context, url, error) => new Icon(Icons.error),
      fadeInDuration: Duration(seconds: 2),
      fadeOutDuration: Duration(seconds: 1),
      height: height,
      width: width,
      fit: BoxFit.fill,
    );

class _HeadlineSlideState extends State<HeadlineSlide> {
  Widget _author(String author) {
    if (author == null ||
        author == "Suara.com" ||
        author == "Republika.co.id") {
      return Container();
    } else {
      return Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(right: 5.0),
            child: Icon(
              Icons.person,
              size: 11.0,
              color: Colors.white,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Text(
              author,
              style: TextStyle(
                color: Colors.white,
                fontSize: 11.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();
    return Material(
      type: MaterialType.transparency,
      child: Container(
        height: 290.0,
        child: Stack(
          children: <Widget>[
            ConstrainedBox(
              constraints: const BoxConstraints(
                  minWidth: double.infinity, minHeight: double.infinity),
              child: _cacheNetworkImage(
                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(this.widget.tag == "headline" ? this.widget.headline.postDateCreated : this.widget.persib.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(this.widget.tag == "headline" ? this.widget.headline.postDateCreated : this.widget.persib.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(this.widget.tag == "headline" ? this.widget.headline.postDateCreated : this.widget.persib.postDateCreated))}/${this.widget.tag == "headline" ? this.widget.headline.postId : this.widget.persib.postId}/${this.widget.tag == "headline" ? this.widget.headline.postImageContent : this.widget.persib.postImageContent}",
                  MediaQuery.of(context).size.height,
                  MediaQuery.of(context).size.width),
            ),
//          Container(
//            height: 270.0,
//            child: Image.asset("assets/image_header.png", fit: BoxFit.fill,),
//          ),
            // this.widget.tag != "headline"
            //     ? Container()
            //     : Container(
            //         height: 40.0,
            //         width: 100.0,
            //         decoration: BoxDecoration(
            //           color: Colors.orange,
            //         ),
            //         child: Center(
            //           child: Text(
            //             "HEADLINE",
            //             style: TextStyle(
            //                 fontSize: 16.0,
            //                 fontWeight: FontWeight.bold,
            //                 color: Colors.white),
            //           ),
            //         ),
            //       ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                height: 90.0,
                decoration:
                    BoxDecoration(color: Colors.indigo.withOpacity(0.8)),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 5.0, left: 3.0, right: 3.0),
                      child: SizedBox(
                        height: 49.0,
                        child: RichText(
                          text: TextSpan(
                            style: DefaultTextStyle.of(context).style,
                            children: <TextSpan>[
                              TextSpan(
                                  text:
                                      "${this.widget.tag == "headline" ? this.widget.headline.postTitle : this.widget.persib.postTitle}",
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),

//                            Text(
//                              "${this.widget.tag == "headline" ? this.widget.headline.postTitle : this.widget.persib.postTitle}",
//                              style: TextStyle(
//                                  fontSize: 20.0,
//                                  fontWeight: FontWeight.bold,
//                                  color: Colors.white),
//                              overflow: TextOverflow.ellipsis,
//                              maxLines: 2,
//                            ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 2.0, right: 2.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: const EdgeInsets.only(right: 5.0),
                                child: Icon(
                                  Icons.today,
                                  size: 11.0,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                "${DateFormat('EEEE', 'id').format(DateTime.parse(this.widget.tag == "headline" ? this.widget.headline.postDate : this.widget.persib.postDate))}, ${DateFormat('dd', 'id').format(DateTime.parse(this.widget.tag == "headline" ? this.widget.headline.postDate : this.widget.persib.postDate))} ${DateFormat('MMMM', 'id').format(DateTime.parse(this.widget.tag == "headline" ? this.widget.headline.postDate : this.widget.persib.postDate))} ${DateFormat('yyyy', 'id').format(DateTime.parse(this.widget.tag == "headline" ? this.widget.headline.postDate : this.widget.persib.postDate))}",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 11.0,
                                ),
                              ),
                            ],
                          ),
                          this.widget.tag == "headline"
                              ? _author(this.widget.headline.author)
                              : _author(this.widget.persib.author)
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
//          Align(
//            alignment: Alignment.bottomCenter,
//            child: Container(
//              padding: const EdgeInsets.only(top: 200.0, left: 50.0, right: 50.0),
//              decoration: BoxDecoration(
//                color: Colors.orange,
//              ),
//              child: Card(
//                color: Colors.indigoAccent,
//                child: Center(
//                  child: Text("SizeBox"),
//                ),
//              ),
//            ),
//          ),
          ],
        ),
      ),
    );
  }
}
