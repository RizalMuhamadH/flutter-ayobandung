
import 'package:firebase_admob/firebase_admob.dart';
import 'package:flutter/material.dart';

class TestPage extends StatefulWidget {
  @override
  _TestPageState createState() => _TestPageState();
}

class _TestPageState extends State<TestPage> {

  //admob
  BannerAd bannerAd;
  InterstitialAd interstitialAd;

  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    keywords: <String>[
      'games',
      'news',
      'event',
      'bandung',
      'jawa barat',
      'ayobandung.com'
    ],
    childDirected: true,
//    nonPersonalizedAds: true,
  );


  BannerAd createBannerAd() {

    return BannerAd(
      adUnitId: BannerAd.testAdUnitId,
      size: AdSize.banner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
//        if(event == MobileAdEvent.loaded){
//          bannerAd..show();
//        } else if (event == MobileAdEvent.clicked){
//
//        }
        print("BannerAd event $event");
      },
    );
  }

//  buildAds(){
//    return Ban
//  }

  InterstitialAd buildInterstitialAd() {
    return InterstitialAd(
        adUnitId: InterstitialAd.testAdUnitId,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print(event);
        });
  }

  @override
  void initState() {
    FirebaseAdMob.instance.initialize(appId: FirebaseAdMob.testAppId);
    bannerAd = createBannerAd()..load();
    // TODO: implement initState
    super.initState();


  }

  @override
  void dispose() {

    bannerAd?.dispose();
    interstitialAd?.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    bannerAd
      ..load()
      ..show();
    return Scaffold(
      body: Container(),
    );
  }
}
