/**
 * Created by Rizal Muhamad H on 7/17/2019 9:35 AM.
 * rizalmuhamadh@gmail.com
 */
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

class DetailsAboutPage extends StatefulWidget {
  String title;
  var text;

  DetailsAboutPage({this.title, this.text});

  @override
  _DetailsAboutPageState createState() => _DetailsAboutPageState();
}

class _DetailsAboutPageState extends State<DetailsAboutPage> {
  Widget _createHtmlView(BuildContext context) {
    return Html(
      data: widget.text,
      padding: const EdgeInsets.only(
        left: 5.0,
      ),
      useRichText: true,
      defaultTextStyle: TextStyle(fontSize: 15, color: Colors.black),
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    debugPrint(widget.text);
    // TODO: Implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        title: Text(
          widget.title.toUpperCase(),
          style: TextStyle(
              color: Colors.white, fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
      ),
      body: SingleChildScrollView(
          child: _createHtmlView(context)),
    );
  }
}
