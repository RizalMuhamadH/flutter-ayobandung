import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import '../scoped/news_category_scoped.dart';
import '../utility/other.dart';
import '../models/article_model.dart';

const KEY = "AIzaSyC_ZpIzTdTFQGRJw2iQudgOA2s5ZzdI-kk";

class PopularPage extends StatefulWidget {

  @override
  _PopularPageState createState() => _PopularPageState();
}


class _PopularPageState extends State<PopularPage> {
  String cat = "0";
  int pageRecent = 0;

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget _cacheNetworkImage(String imageUrl) => CachedNetworkImage(
    imageUrl: imageUrl,
    placeholder: (context, url) => Image.asset('assets/loading_image.png'),
    errorWidget: (context, url, error) => new Icon(Icons.error),
    fadeInDuration: Duration(seconds: 1),
    fadeOutDuration: Duration(seconds: 1),
    fit: BoxFit.fill,
  );

  Widget _buildFutureView() => ScopedModelDescendant<NewsCategoryScoped>(
    builder: (context, child, model){

      return FutureBuilder(
          future: model.fetchgetArticle(cat, pageRecent),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(child: CircularProgressIndicator());
              case ConnectionState.done:
                return _createListView(model.article);
              default:
                if (snapshot.hasError)
                  return Center(child: Text("Error : ${snapshot.error}"));
            }
          });
    },
  );

  _createListView(Article article) {
    return ScopedModelDescendant<NewsCategoryScoped>(
      rebuildOnChange: true,
      builder: (context, child, model){
        return ListView.builder(
            scrollDirection: Axis.vertical,
            itemCount: article.data.length,
            itemBuilder: (BuildContext context, int index) {
              if (index >= article.data.length - 1) {
                print(article.data[index].postTitle);
                pageRecent++;
                model.fetchgetArticle(cat, pageRecent);

              }

              return Padding(
                padding: EdgeInsets.only(left: 2.0, right: 2.0),
                child: Column(
                  children: <Widget>[
                    InkWell(
                      onTap: () {
//                          Navigator.of(context).push(MaterialPageRoute(
//                              builder: (BuildContext context) =>
//                                  ContentPage(
//                                    id: article.data[index].postId,
//                                    tag: "category_$index",
//                                    category: article.data[index].categoryName,
//                                    image:
//                                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(
//                                        DateTime.parse(article.data[index]
//                                            .postDateCreated))}/${DateFormat('MM')
//                                        .format(DateTime.parse(article.data[index]
//                                        .postDateCreated))}/${DateFormat('dd')
//                                        .format(DateTime.parse(article.data[index]
//                                        .postDateCreated))}/${article.data[index]
//                                        .postId}/${article.data[index]
//                                        .postImageContent}",
//                                  )));
                      },
                      child: Hero(
                          tag: "category_$index",
                          child: _buildListItem(model.article.data[index])),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Divider(
                        height: 1.0,
                        color: Colors.black26,
                      ),
                    ),
                  ],
                ),
              );
            });
      },
    );
  }

  Widget _buildListItem(Data item) => Material(
    child: Container(
      height: 100.0,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 5.0,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(6.0),
              child: Container(
                width: 100.0,
                height: 100.0,
                child: _cacheNetworkImage(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}"),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 2.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: const EdgeInsets.only(right: 5.0),
                          child: Icon(
                            Icons.category,
                            size: 11.0,
                            color: Colors.blueAccent,
                          ),
                        ),
                        Text(
                          item.categoryName,
                          style: TextStyle(
                            fontSize: 11.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 1.0),
                      child: SizedBox(
                        height: 38.0,
                        child: Text(
                          item.postTitle,
                          style: TextStyle(
                            fontSize: 14.0,
                            fontWeight: FontWeight.bold,
                          ),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 5.0),
                    child: RichText(
                      text: TextSpan(
                        style: DefaultTextStyle.of(context).style,
                        children: <TextSpan>[
                          item.author == null
                              ? TextSpan()
                              : TextSpan(children: <TextSpan>[
                            TextSpan(
                              text: "Oleh ",
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                            TextSpan(
                                text: "${item.author} ",
                                style: TextStyle(
                                  fontSize: 11.0,
                                  fontWeight: FontWeight.bold,
                                )),
                          ]),
                          TextSpan(
                            text: _getDateBetween(item.postDate),
                            style: TextStyle(
                              fontSize: 11.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
                margin: const EdgeInsets.only(top: 8.0, right: 3.0),
                child: GestureDetector(
                  onTap: () {
//                    Share.share(
//                        "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.slug}");
                  },
                  child: Icon(
                    Icons.more_vert,
                    size: 21.0,
                    color: Colors.black54,
                  ),
                )),
          ],
        ),
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ScopedModel<NewsCategoryScoped>(
        model: NewsCategoryScoped(),
        child: _buildFutureView(),
      ),
    );
  }
}
