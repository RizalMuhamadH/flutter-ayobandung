import 'package:ayobandung/bloc/analytics/bloc.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../pages/content_page.dart';
import '../utility/other.dart';
import '../models/article_model.dart';

class SearchPage extends SearchDelegate<String> {
  final FirebaseAnalytics analytics;
  SearchPage({this.analytics});

  Article _article;
  int _totalArticle = 0;
  final AnalyticsBloc _bloc = new AnalyticsBloc();

  Future<Article> _fetchGetSearch(String keyword) async {
    Response response = await Dio().post(
      "https://www.ayobandung.com/api_mob_new/getSearch",
      data: FormData.fromMap({"search": keyword}),
    );

    //analytics
    _bloc.dispatch(
        LogSearch(analytics: analytics, keyword: keyword, date: getDatenow()));

    var decodeJson = jsonDecode(response.toString());

//    if (identical(Article.fromJson(decodeJson).kode, 200)) {
    _article = Article.fromJson(decodeJson);
//    }

    return _article;
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    return FutureBuilder(
        future: _fetchGetSearch(query),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return _article.data == null
                  ? Container(
                      child: Center(
                        child: Text(
                          "data tidak ditemukan",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : _createListView(_article, context);
            default:
              if (snapshot.hasError)
                return Center(child: Text("Error : ${snapshot.error}"));
          }
          return Container();
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return FutureBuilder(
        future: _fetchGetSearch(query),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
            case ConnectionState.done:
              return _article.data == null
                  ? Container(
                      child: Center(
                        child: Text(
                          "data tidak ditemukan",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  : _createListView(_article, context);
            default:
              if (snapshot.hasError)
                return Center(child: Text("Error : ${snapshot.error}"));
          }

          return Container();
        });
  }

  @override
  void showResults(BuildContext context) {
    print("result 1");
//    showResults(context);
    buildResults(context);
  }

  Widget _cacheNetworkImage(String imageUrl, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: BoxFit.fill,
      );

  String getDatenow() {
    DateTime now = DateTime.now();
    String format = DateFormat("yyyy-MM-dd").format(now);

    return format;
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget _createListView(Article article, BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: _article.data.length,
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ContentPage(
                              analytics: analytics,
                              id: article.data[index].postId,
                              tag: "category_$index",
                              category: article.data[index].categoryName,
                              image:
                                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.data[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.data[index].postDateCreated))}/${article.data[index].postId}/${article.data[index].postImageContent}",
                            )));
                  },
                  child: Hero(
                      tag: "category_$index",
                      child: _buildListItem(article.data[index], context)),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildListItem(Data item, BuildContext context) => Material(
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(top: 5.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(6.0),
                child: Container(
                  width: 100.0,
                  height: 100.0,
                  child: _cacheNetworkImage(
                      "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                      100.0,
                      100.0),
                ),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 5.0),
                            child: Icon(
                              Icons.category,
                              size: 11.0,
                              color: Colors.blueAccent,
                            ),
                          ),
                          Text(
                            item.categoryName,
                            style: TextStyle(
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            item.postTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 5.0),
                      child: RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            _textSpanReporter(item.author),
                            TextSpan(
                              text: _getDateBetween(item.postDate),
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 8.0, right: 3.0),
                  child: InkWell(
                    onTap: () {
                      Share.share(
                          "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.slug}");
                    },
                    child: Icon(
                      Icons.share,
                      size: 21.0,
                      color: Colors.black54,
                    ),
                  )),
            ],
          ),
        ),
      );

  _textSpanReporter(String reporter) {
    if (reporter == null ||
        reporter == "Suara.com" ||
        reporter == "Republika.co.id") {
      return TextSpan();
    } else {
      return TextSpan(children: <TextSpan>[
        TextSpan(
          text: "Oleh ",
          style: TextStyle(
            fontSize: 11.0,
          ),
        ),
        TextSpan(
            text: "${reporter.toUpperCase()} ",
            style: TextStyle(
              fontSize: 11.0,
              fontWeight: FontWeight.bold,
            )),
      ]);
    }
  }
}
