import 'dart:io';

import 'package:admob_flutter/admob_flutter.dart';
import 'package:ayobandung/bloc/analytics/bloc.dart';
import 'package:ayobandung/models/ads.dart';
import 'package:ayobandung/models/article.dart';
import 'package:ayobandung/models/object/Persib.dart';
import 'package:ayobandung/models/object/Photo.dart';
import 'package:ayobandung/models/object/Video.dart';
import 'package:ayobandung/models/object/headline.dart';
import 'package:ayobandung/pages/test.dart';
import 'package:ayobandung/utility/adsense.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import './headline_slide.dart';
import 'package:page_indicator/page_indicator.dart';
import 'package:dio/dio.dart';
import 'dart:convert';
import 'package:share/share.dart';
import '../utility/other.dart';
import 'package:intl/intl.dart';
import 'package:connectivity/connectivity.dart';
import 'dart:async';
import 'package:url_launcher/url_launcher.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/services.dart';
import './content_page.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import './photo_page.dart';
import 'package:firebase_admob/firebase_admob.dart';

const KEY = "AIzaSyC_ZpIzTdTFQGRJw2iQudgOA2s5ZzdI-kk";

class ArticlePage extends StatefulWidget {
  final FirebaseAnalytics analytics;

  const ArticlePage({Key key, this.analytics}) : super(key: key);
  @override
  _ArticlePageState createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage> {
  String _connectionStatus = 'Unknown';
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;
  bool _status = false;

  final snackBar = SnackBar(content: Text("Koneksi internet terputus"));

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

//  Article headline;
//  AllModel allModel;
  List<Headline> _listHeadline;
  List<Persib> _listPersib;
  List<Article> _listRecent;
  List<Article> _listPopuler;
  List<Article> _listNetizen;
  List<Photo> _listPhoto;
  List<Video> _listVideo;

  Ads _ads;

  int _totalHeadline = 0;
  int _totalRecent = 0;
  int _totalPopular = 0;
  int _totalPersib = 0;
  int _totalNetizen = 0;
  int _totalPhoto = 0;
  int _totalVideo = 0;

  int heroTag = 0;

  //analytics bloc
  final AnalyticsBloc _bloc = new AnalyticsBloc();

  //admob
  BannerAd bannerAd;
  InterstitialAd interstitialAd;

  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    contentUrl: 'https://www.ayobandung.com/',
    keywords: <String>[
      'games',
      'news',
      'event',
      'bandung',
      'jawa barat',
      'ayobandung.com'
    ],
    childDirected: true,
//    nonPersonalizedAds: true,
  );

  BannerAd createBannerAd() {
    return BannerAd(
      adUnitId: 'ca-app-pub-6344910443143463/1486667833',
      size: AdSize.banner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
//        if(event == MobileAdEvent.loaded){
//          bannerAd..show();
//        } else if (event == MobileAdEvent.clicked){
//
//        }
        print("BannerAd event $event");
      },
    );
  }

//  buildAds(){
//    return Ban
//  }

  InterstitialAd buildInterstitialAd() {
    return InterstitialAd(
        adUnitId: InterstitialAd.testAdUnitId,
        targetingInfo: targetingInfo,
        listener: (MobileAdEvent event) {
          print(event);
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bloc.dispatch(
        LogViewItemList(analytics: widget.analytics, category: "Home"));
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);

    // FirebaseAdMob.instance
    // //     .initialize(appId: 'ca-app-pub-6344910443143463~1317833791');
    // Admob.initialize('ca-app-pub-6344910443143463~1317833791');
    // bannerAd = createBannerAd()..load();
//    interstitialAd = buildInterstitialAd()..load();

    _fetchGetRecent();
    _fetchGetPopular();
    _fetchGetPersib();
    _fetchGetNetizen();
    _fetchGetPhoto();
    _fetchGetVideo();
  }

  Future<void> initConnectivity() async {
    ConnectivityResult connectionStatus;

    try {
      connectionStatus = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      _connectionStatus = "Internet connectivity failed";
    }

    if (!mounted) {
      return;
    }

    _updateConnectionStatus(connectionStatus);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    _connectionStatus = result.toString();
    print("InitConnectivity : $_connectionStatus");
    if (result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi) {
      setState(() {
        _status = true;
      });
    } else {
      _status = true;
//      _scaffoldKey.currentState.showSnackBar(snackBar);
      print("You are not connected to internet");
    }
  }

//  Future<AllModel> _fetchGetAllNews() async {
//    Response response = await Dio().get("${Other.BASE_URL}getAll");
//    var decodeJson = jsonDecode(response.toString());
//
//    allModel = AllModel.fromJson(decodeJson);
//
//    print(allModel.photo.length);
//
//    _totalRecent = allModel.recent.length;
//    _totalPersib = allModel.persib.length;
//    _totalPopular = allModel.popular.length;
//    _totalHeadline = allModel.headline.length;
//    _totalNetizen = allModel.netizen.length;
//    return allModel;
//  }

  Future<List<Headline>> _fetchGetHeadline() async {
    Response response = await Dio().get("${Other.BASE_URL}headline");
    var rest = jsonDecode(response.toString());

    _ads = Ads.fromJson(rest);

    var json = rest["headline"] as List;

    _listHeadline =
        json.map<Headline>((json) => Headline.fromJson(json)).toList();

    setState(() {
      _totalHeadline = _listHeadline.length;
    });

    return _listHeadline;
  }

  Future<List<Persib>> _fetchGetPersib() async {
    Response response = await Dio().get("${Other.BASE_URL}persib");
    var rest = jsonDecode(response.toString());

    var json = rest["persib"] as List;

    _listPersib = json.map<Persib>((json) => Persib.fromJson(json)).toList();

    setState(() {
      _totalPersib = _listPersib.length;
    });

    return _listPersib;
  }

  Future<List<Article>> _fetchGetRecent() async {
    Response response = await Dio().get("${Other.BASE_URL}news");
    var rest = jsonDecode(response.toString());

    var json = rest["recent"] as List;

    _listRecent = json.map<Article>((json) => Article.fromJson(json)).toList();

    setState(() {
      _totalRecent = _listRecent.length;
    });

    print("Total Recent ${_totalRecent}");

    return _listRecent;
  }

  Future<List<Article>> _fetchGetPopular() async {
    Response response = await Dio().get("${Other.BASE_URL}popular");
    var rest = jsonDecode(response.toString());

    var json = rest["popular"] as List;

    _listPopuler = json.map<Article>((json) => Article.fromJson(json)).toList();

    setState(() {
      _totalPopular = _listPopuler.length;
    });

    return _listPopuler;
  }

  Future<List<Article>> _fetchGetNetizen() async {
    Response response = await Dio().get("${Other.BASE_URL}netizen");
    var rest = jsonDecode(response.toString());

    var json = rest["netizen"] as List;

    _listNetizen = json.map<Article>((json) => Article.fromJson(json)).toList();

    setState(() {
      _totalNetizen = _listNetizen.length;
    });

    return _listNetizen;
  }

  Future<List<Photo>> _fetchGetPhoto() async {
    Response response = await Dio().get("${Other.BASE_URL}photo");
    var rest = jsonDecode(response.toString());

    var json = rest["photo"] as List;

    _listPhoto = json.map<Photo>((json) => Photo.fromJson(json)).toList();

    setState(() {
      _totalPhoto = _listPhoto.length;
    });

    return _listPhoto;
  }

  Future<List<Video>> _fetchGetVideo() async {
    Response response = await Dio().get("${Other.BASE_URL}video");
    var rest = jsonDecode(response.toString());

    var json = rest["video"] as List;

    _listVideo = json.map<Video>((json) => Video.fromJson(json)).toList();

    setState(() {
      _totalVideo = _listVideo.length;
    });

    return _listVideo;
  }

  String _getDateBetween(String date) {
    final dateParse = DateTime.parse(date);
    final dateToday = DateTime.now();
    final difference = dateToday.difference(dateParse).inSeconds;

    int second = difference;
    int minute = (second / 60).round();
    int hour = (second / 3600).round();
    int day = (second / 86400).round();
    int week = (second / 604800).round();
    int mounth = (second / 2419200).round();
    int year = (second / 29030400).round();

    if (second <= 60) {
      return "$second detik yang lalu";
    } else if (minute <= 60) {
      return "$minute menit yang lalu";
    } else if (hour <= 24) {
      return "$hour jam yang lalu";
    } else if (day <= 7) {
      return "$day hari yang lalu";
    } else if (week <= 4) {
      return "$week minggu yang lalu";
    } else if (mounth <= 12) {
      return "$mounth bulan yang lalu";
    } else {
      return "$year tahun yang lalu";
    }
  }

  Widget _cacheNetworkImage(
          String imageUrl, BoxFit fit, double height, double width) =>
      CachedNetworkImage(
        imageUrl: imageUrl,
        placeholder: (context, url) => Image.asset('assets/loading_image.png'),
        errorWidget: (context, url, error) => new Icon(Icons.error),
        fadeInDuration: Duration(seconds: 1),
        fadeOutDuration: Duration(seconds: 1),
        height: height,
        width: width,
        fit: fit,
      );

  Widget _buildHeadlineView(String tag) {
    return PageIndicatorContainer(
      align: IndicatorAlign.bottom,
      length: tag == "headline" ? _totalHeadline : _totalPersib,
      indicatorColor: Colors.white,
      indicatorSelectorColor: Colors.red,
      size: 5.0,
      indicatorSpace: 5.0,
      pageView: PageView.builder(
          controller: PageController(),
          scrollDirection: Axis.horizontal,
          itemCount: tag == "headline" ? _totalHeadline : _totalPersib,
          itemBuilder: (BuildContext context, int index) {
            return InkWell(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (BuildContext context) => new ContentPage(
                          analytics: widget.analytics,
                          id: tag == "headline"
                              ? _listHeadline[index].postId
                              : _listPersib[index].postId,
                          tag: tag == "headline"
                              ? "headline_$index"
                              : "persib_$index",
                          category: tag == "headline"
                              ? _listHeadline[index].categoryName
                              : _listHeadline[index].categoryName,
                          image:
                              "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(tag == "headline" ? _listHeadline[index].postDateCreated : _listPersib[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(tag == "headline" ? _listHeadline[index].postDateCreated : _listPersib[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(tag == "headline" ? _listHeadline[index].postDateCreated : _listPersib[index].postDateCreated))}/${tag == "headline" ? _listHeadline[index].postId : _listPersib[index].postId}/${tag == "headline" ? _listHeadline[index].postImageContent : _listPersib[index].postImageContent}",
                        )));
              },
              child: Hero(
                tag: tag == "headline" ? "headline_$index" : "persib_$index",
                child: tag == "headline"
                    ? HeadlineSlide(
                        headline: _listHeadline[index],
                        tag: tag,
                      )
                    : HeadlineSlide(
                        persib: _listPersib[index],
                        tag: tag,
                      ),
              ),
            );
          }),
    );
  }

  Widget _buildTitle(Icon icon, String title) => Container(
        child: Padding(
          padding: const EdgeInsets.only(
              left: 2.0, top: 8.0, right: 2.0, bottom: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 6.0),
                child: icon,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: Text(
                  title,
                  style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ),
        ),
      );

  Widget _createListView(List<Article> article) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: article.length,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return Padding(
            padding: EdgeInsets.only(left: 2.0, right: 2.0),
            child: Column(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) => ContentPage(
                              analytics: widget.analytics,
                              id: article[index].postId,
                              tag: article[index].postId,
                              category: article[index].categoryName,
                              image:
                                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article[index].postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article[index].postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article[index].postDateCreated))}/${article[index].postId}/${article[index].postImageContent}",
                            )));
                  },
                  child: Hero(
                      tag: article[index].postId,
                      child: _buildListItem(article[index])),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 5.0),
                  child: Divider(
                    height: 1.0,
                    color: Colors.black26,
                  ),
                ),
              ],
            ),
          );
        });
  }

  _textSpanReporter(String reporter) {
    if (reporter == null ||
        reporter == "Suara.com" ||
        reporter == "Republika.co.id") {
      return TextSpan();
    } else {
      return TextSpan(children: <TextSpan>[
        TextSpan(
          text: "Oleh ",
          style: TextStyle(
            fontSize: 11.0,
          ),
        ),
        TextSpan(
            text: "${reporter.toUpperCase()} ",
            style: TextStyle(
              fontSize: 11.0,
              fontWeight: FontWeight.bold,
            )),
      ]);
    }
  }

  Widget _buildListItem(Article article) => Material(
        child: Container(
          height: 100.0,
          padding: const EdgeInsets.only(
            top: 5.0,
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(6.0),
                child: _cacheNetworkImage(
                    "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(article.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.postDateCreated))}/${article.postId}/${article.postImageContent}",
                    BoxFit.fill,
                    100.0,
                    100.0),
              ),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(right: 5.0),
                            child: Icon(
                              Icons.category,
                              size: 11.0,
                              color: Colors.blueAccent,
                            ),
                          ),
                          Text(
                            article.categoryName.toUpperCase(),
                            style: TextStyle(
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.only(right: 1.0),
                        child: SizedBox(
                          height: 38.0,
                          child: Text(
                            article.postTitle,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 10.0, right: 2.0),
                      child: RichText(
                        text: TextSpan(
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            _textSpanReporter(article.author),
                            TextSpan(
                              text: _getDateBetween(article.postDate),
                              style: TextStyle(
                                fontSize: 11.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  margin: const EdgeInsets.only(top: 8.0, right: 3.0),
                  child: InkWell(
                    onTap: () {
                      Share.share(
                          "${Other.URL_WEBSITE}${DateFormat('yyyy').format(DateTime.parse(article.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(article.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(article.postDateCreated))}/${article.postId}/${article.slug}");
                    },
                    child: Icon(
                      Icons.share,
                      size: 21.0,
                      color: Colors.black54,
                    ),
                  )),
            ],
          ),
        ),
      );

  Widget _buildFotoSlider(List<Photo> listFoto) => CarouselSlider(
        items: listFoto == null
            ? <Widget>[
                Center(
                  child: CircularProgressIndicator(),
                ),
              ]
            : listFoto
                .map(
                  (item) => _createFotoItem(item),
                )
                .toList(),
        autoPlay: false,
        height: 240.0,
        aspectRatio: 2.0,
        initialPage: 0,
        viewportFraction: 0.8,
      );

  Widget _createFotoItem(Photo item) {
    heroTag += 1;
    return Material(
      elevation: 15.0,
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => PhotoPage(item.postId)));
        },
        child: Stack(
          children: <Widget>[
            Hero(
              tag: heroTag,
              child: _cacheNetworkImage(
                  "${Other.URL_IMAGE}${DateFormat('yyyy').format(DateTime.parse(item.postDateCreated))}/${DateFormat('MM').format(DateTime.parse(item.postDateCreated))}/${DateFormat('dd').format(DateTime.parse(item.postDateCreated))}/${item.postId}/${item.postImageContent}",
                  BoxFit.fill,
                  240.0,
                  MediaQuery.of(context).size.width),
            ),
            Positioned(
              left: 5.0,
              right: 5.0,
              bottom: 2.0,
              child: Container(
                  height: 35.0,
                  decoration:
                      BoxDecoration(color: Colors.black.withOpacity(0.8)),
                  child: Text(
                    item.postTitle,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.white),
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildVideoGridView(List<Video> listVideo) => GridView.count(
        crossAxisCount: 3,
        physics: const NeverScrollableScrollPhysics(),
        children: listVideo.map((item) => _createVideoItem(item)).toList(),
      );

  Widget _createVideoItem(Video item) {
    return Container(
      height: 200.0,
      padding: const EdgeInsets.all(3.0),
      child: GestureDetector(
        onTap: () {
          FlutterYoutube.playYoutubeVideoById(
            apiKey: KEY,
            videoId: item.video,
            autoPlay: true,
            fullScreen: false,
          );
        },
        child: _cacheNetworkImage(
            "https://img.youtube.com/vi/${item.video}/mqdefault.jpg",
            BoxFit.fill,
            200.0,
            MediaQuery.of(context).size.width),
      ),
    );
  }

  Widget _sliverCustom() {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          automaticallyImplyLeading: false,
          expandedHeight: 290.0,
          floating: false,
          backgroundColor: Colors.white,
          flexibleSpace: FlexibleSpaceBar(
            background: Stack(
              children: <Widget>[
                _buildHeadlineView("headline"),
              ],
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              _spaceAds(_ads.imageAds1, _ads.adsLink1, _ads.adsSize1),
              _buildTitle(
                  Icon(
                    Icons.history,
                    color: Colors.deepPurple,
                  ),
                  "BERITA TERBARU"),
              !Platform.isIOS
                  ? AdmobBanner(
                      adUnitId: Adsense.BANNER_ID_1,
                      adSize: AdmobBannerSize.BANNER,
                      listener:
                          (AdmobAdEvent event, Map<String, dynamic> args) {
                        print('TEST $event $args');
                      },
                    )
                  : Container(),
              _totalRecent != 0 ? _createListView(_listRecent) : Container(),
              _spaceAds(_ads.imageAds2, _ads.adsLink2, _ads.adsSize2),
              _buildTitle(
                  Icon(
                    Icons.whatshot,
                    color: Colors.indigo,
                  ),
                  "AYO PERSIB"),
              _totalPersib != 0
                  ? Container(
                      height: 290.0, child: _buildHeadlineView("persib"))
                  : Container(),
              _spaceAds(_ads.imageAds3, _ads.adsLink3, _ads.adsSize3),
              _buildTitle(
                  Icon(
                    Icons.book,
                    color: Colors.teal,
                  ),
                  "BERITA TERPOPULER"),
              _totalPopular != 0 ? _createListView(_listPopuler) : Container(),
              _spaceAds(_ads.imageAds4, _ads.adsLink4, _ads.adsSize4),
              _buildTitle(
                  Icon(
                    Icons.photo,
                    color: Colors.purple,
                  ),
                  "AYO FOTO"),
              _totalPhoto != 0 ? _buildFotoSlider(_listPhoto) : Container(),
              _buildTitle(
                  Icon(
                    Icons.supervised_user_circle,
                    color: Colors.teal,
                  ),
                  "NETIZEN"),
              _totalNetizen != 0 ? _createListView(_listNetizen) : Container(),
              _spaceAds(_ads.imageAds5, _ads.adsLink5, _ads.adsSize5),
              _buildTitle(
                  Icon(
                    Icons.ondemand_video,
                    color: Colors.purple,
                  ),
                  "AYO VIDEO"),
              _totalVideo != 0
                  ? Container(
                      height: 278.0, child: _buildVideoGridView(_listVideo))
                  : Container(),
            ],
          ),
        ),
      ],
    );
  }

  Widget _spaceAds(String url, String link, double height) {
    return url == null
        ? Container()
        : Container(
            height: height,
            margin: EdgeInsets.only(
              top: 5.0,
              bottom: 5.0,
            ),
            width: MediaQuery.of(context).size.width,
            child: InkWell(
              child: _cacheNetworkImage(
                  url, BoxFit.fill, height, MediaQuery.of(context).size.width),
              onTap: () {
                _launchURL(link);
              },
            ),
          );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Could not launch $url";
    }
  }

  Widget _widgetContent() {
    return NestedScrollView(
      headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
        return <Widget>[
          SliverAppBar(
            automaticallyImplyLeading: false,
            expandedHeight: 290.0,
            floating: false,
            backgroundColor: Colors.white,
            flexibleSpace: FlexibleSpaceBar(
              background: Stack(
                children: <Widget>[
//                    _buildTitle(),
                  _buildHeadlineView("headline"),
//                  HeadlineSlide(),
                ],
              ),
            ),
          ),
        ];
      },
      body: Center(
        child: ListView(
          children: <Widget>[
//            _spaceAds(allModel.imageAds1),
            _buildTitle(
                Icon(
                  Icons.history,
                  color: Colors.deepPurple,
                ),
                "BERITA TERBARU"),
//            Container(
//                height: 1080.0, child: _createListView(allModel, "terkini")),
//            _spaceAds(allModel.imageAds2),
            _buildTitle(
                Icon(
                  Icons.whatshot,
                  color: Colors.indigo,
                ),
                "AYO PERSIB"),
//            Container(height: 290.0, child: _buildHeadlineView("persib")),
//            _spaceAds(allModel.imageAds3),
            _buildTitle(
                Icon(
                  Icons.book,
                  color: Colors.teal,
                ),
                "BERITA TERPOPULER"),
//            Container(
//                height: 1075.0, child: _createListView(allModel, "terpopuler")),
//            _spaceAds(allModel.imageAds4),
            _buildTitle(
                Icon(
                  Icons.photo,
                  color: Colors.purple,
                ),
                "AYO FOTO"),
//            _buildFotoSlider(allModel),
//            _spaceAds(allModel.imageAds5),
            _buildTitle(
                Icon(
                  Icons.ondemand_video,
                  color: Colors.purple,
                ),
                "AYO VIDEO"),
//            Container(height: 278.0, child: _buildVideoGridView(allModel)),
          ],
        ),
      ),
    );
  }

  Widget _futureBuilderView() => FutureBuilder(
      future: _fetchGetHeadline(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(child: CircularProgressIndicator());
          case ConnectionState.done:
            return _listHeadline == null ? Container() : _sliverCustom();
          default:
            if (snapshot.hasError)
              return Center(child: Text("Error : ${snapshot.error}"));
        }
        return Container();
      });

  @override
  Widget build(BuildContext context) {
    Admob.initialize(Adsense.APP_ID);
    // bannerAd
    //   ..load()
    //   ..show();

//    interstitialAd..load()..show();

    return Scaffold(
      key: _scaffoldKey,
      body: _status == true
          ? _listHeadline == null ? _futureBuilderView() : _sliverCustom()
          : Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.signal_wifi_off),
                    Text(
                      "Connection Failed",
                      style: TextStyle(fontFamily: 'Montserrat'),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _connectivitySubscription.cancel();
    bannerAd?.dispose();
    interstitialAd?.dispose();
    super.dispose();
  }
}
