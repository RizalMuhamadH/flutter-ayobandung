class UserNetizen {
  String _netId;
  String _netNama;
  String _netPhoto;
  String _netDeskripsi;
  String _postId;

  UserNetizen(
      {String netId,
        String netNama,
        String netPhoto,
        String netDeskripsi,
        String postId}) {
    this._netId = netId;
    this._netNama = netNama;
    this._netPhoto = netPhoto;
    this._netDeskripsi = netDeskripsi;
    this._postId = postId;
  }

  String get netId => _netId;
  set netId(String netId) => _netId = netId;
  String get netNama => _netNama;
  set netNama(String netNama) => _netNama = netNama;
  String get netPhoto => _netPhoto;
  set netPhoto(String netPhoto) => _netPhoto = netPhoto;
  String get netDeskripsi => _netDeskripsi;
  set netDeskripsi(String netDeskripsi) => _netDeskripsi = netDeskripsi;
  String get postId => _postId;
  set postId(String postId) => _postId = postId;

  UserNetizen.fromJson(Map<String, dynamic> json) {
    _netId = json['net_id'];
    _netNama = json['net_nama'];
    _netPhoto = json['net_photo'];
    _netDeskripsi = json['net_deskripsi'];
    _postId = json['post_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['net_id'] = this._netId;
    data['net_nama'] = this._netNama;
    data['net_photo'] = this._netPhoto;
    data['net_deskripsi'] = this._netDeskripsi;
    data['post_id'] = this._postId;
    return data;
  }
}
