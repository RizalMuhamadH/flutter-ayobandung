import 'package:ayobandung/models/article.dart';

class AllModel {
  String _status;
  int _kode;
  List<Headline> _headline;
  List<Article> _recent;
  List<Article> _popular;
  List<Persib> _persib;
  List<Article> _netizen;
  List<Photo> _photo;
  List<Video> _video;
  String _imageAds1;
  String _imageAds2;
  String _imageAds3;
  String _imageAds4;
  String _imageAds5;
  String _adsLink1;
  String _adsLink2;
  String _adsLink3;
  String _adsLink4;
  String _adsLink5;
  double _adsSize1;
  double _adsSize2;
  double _adsSize3;
  double _adsSize4;
  double _adsSize5;

  AllModel(
      {String status,
      int kode,
      List<Headline> headline,
      List<Article> recent,
      List<Article> popular,
      List<Persib> persib,
      List<Article> netizen,
      List<Photo> photo,
      List<Video> video,
      String imageAds1,
      String imageAds2,
      String imageAds3,
      String imageAds4,
      String imageAds5,
      String adsLink1,
      String adsLink2,
      String adsLink3,
      String adsLink4,
      String adsLink5,
      double adsSize1,
      double adsSize2,
      double adsSize3,
      double adsSize4,
      double adsSize5}) {
    this._status = status;
    this._kode = kode;
    this._headline = headline;
    this._recent = recent;
    this._popular = popular;
    this._persib = persib;
    this._netizen = netizen;
    this._photo = photo;
    this._video = video;
    this._imageAds1 = imageAds1;
    this._imageAds2 = imageAds2;
    this._imageAds3 = imageAds3;
    this._imageAds4 = imageAds4;
    this._imageAds5 = imageAds5;
    this._adsLink1 = adsLink1;
    this._adsLink2 = adsLink2;
    this._adsLink3 = adsLink3;
    this._adsLink4 = adsLink4;
    this._adsLink5 = adsLink5;
    this._adsSize1 = adsSize1;
    this._adsSize2 = adsSize2;
    this._adsSize3 = adsSize3;
    this._adsSize4 = adsSize4;
    this._adsSize5 = adsSize5;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Headline> get headline => _headline;
  set headline(List<Headline> headline) => _headline = headline;
  List<Article> get recent => _recent;
  set recent(List<Article> recent) => _recent = recent;
  List<Article> get popular => _popular;
  set popular(List<Article> popular) => _popular = popular;
  List<Persib> get persib => _persib;
  set persib(List<Persib> persib) => _persib = persib;
  List<Article> get netizen => _netizen;
  set netizen(List<Article> netizen) => _netizen = netizen;
  List<Photo> get photo => _photo;
  set photo(List<Photo> photo) => _photo = photo;
  List<Video> get video => _video;
  set video(List<Video> video) => _video = video;
  String get imageAds1 => _imageAds1;
  set imageAds1(String imageAds1) => _imageAds1 = imageAds1;
  String get imageAds2 => _imageAds2;
  set imageAds2(String imageAds2) => _imageAds2 = imageAds2;
  String get imageAds3 => _imageAds3;
  set imageAds3(String imageAds3) => _imageAds3 = imageAds3;
  String get imageAds4 => _imageAds4;
  set imageAds4(String imageAds4) => _imageAds4 = imageAds4;
  String get imageAds5 => _imageAds5;
  set imageAds5(String imageAds5) => _imageAds5 = imageAds5;
  String get adsLink1 => _adsLink1;
  set adsLink1(String adsLink1) => _adsLink1 = adsLink1;
  String get adsLink2 => _adsLink2;
  set adsLink2(String adsLink2) => _adsLink2 = adsLink2;
  String get adsLink3 => _adsLink3;
  set adsLink3(String adsLink3) => _adsLink3 = adsLink3;
  String get adsLink4 => _adsLink4;
  set adsLink4(String adsLink4) => _adsLink4 = adsLink4;
  String get adsLink5 => _adsLink5;
  set adsLink5(String adsLink5) => _adsLink5 = adsLink5;
  double get adsSize1 => _adsSize1;
  set adsSize1(double adsSize1) => _adsSize1 = adsSize1;
  double get adsSize2 => _adsSize2;
  set adsSize2(double adsSize2) => _adsSize2 = adsSize2;
  double get adsSize3 => _adsSize3;
  set adsSize3(double adsSize3) => _adsSize3 = adsSize3;
  double get adsSize4 => _adsSize4;
  set adsSize4(double adsSize4) => _adsSize4 = adsSize4;
  double get adsSize5 => _adsSize5;
  set adsSize5(double adsSize5) => _adsSize5 = adsSize5;

  AllModel.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['headline'] != null) {
      _headline = new List<Headline>();
      json['headline'].forEach((v) {
        _headline.add(new Headline.fromJson(v));
      });
    }
    if (json['recent'] != null) {
      _recent = new List<Article>();
      json['recent'].forEach((v) {
        _recent.add(new Article.fromJson(v));
      });
    }
    if (json['popular'] != null) {
      _popular = new List<Article>();
      json['popular'].forEach((v) {
        _popular.add(new Article.fromJson(v));
      });
    }
    if (json['persib'] != null) {
      _persib = new List<Persib>();
      json['persib'].forEach((v) {
        _persib.add(new Persib.fromJson(v));
      });
    }
    if (json['netizen'] != null) {
      _netizen = new List<Article>();
      json['netizen'].forEach((v) {
        _netizen.add(new Article.fromJson(v));
      });
    }
    if (json['photo'] != null) {
      _photo = new List<Photo>();
      json['photo'].forEach((v) {
        _photo.add(new Photo.fromJson(v));
      });
    }
    if (json['video'] != null) {
      _video = new List<Video>();
      json['video'].forEach((v) {
        _video.add(new Video.fromJson(v));
      });
    }
    _imageAds1 = json['image_ads_1'];
    _imageAds2 = json['image_ads_2'];
    _imageAds3 = json['image_ads_3'];
    _imageAds4 = json['image_ads_4'];
    _imageAds5 = json['image_ads_5'];
    _adsLink1 = json['ads_link_1'];
    _adsLink2 = json['ads_link_2'];
    _adsLink3 = json['ads_link_3'];
    _adsLink4 = json['ads_link_4'];
    _adsLink5 = json['ads_link_5'];
    _adsSize1 = json['ads_size_1'];
    _adsSize2 = json['ads_size_2'];
    _adsSize3 = json['ads_size_3'];
    _adsSize4 = json['ads_size_4'];
    _adsSize5 = json['ads_size_5'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._headline != null) {
      data['headline'] = this._headline.map((v) => v.toJson()).toList();
    }
    if (this._recent != null) {
      data['recent'] = this._recent.map((v) => v.toJson()).toList();
    }
    if (this._popular != null) {
      data['popular'] = this._popular.map((v) => v.toJson()).toList();
    }
    if (this._persib != null) {
      data['persib'] = this._persib.map((v) => v.toJson()).toList();
    }
    if (this._netizen != null) {
      data['netizen'] = this._netizen.map((v) => v.toJson()).toList();
    }
    if (this._photo != null) {
      data['photo'] = this._photo.map((v) => v.toJson()).toList();
    }
    if (this._video != null) {
      data['video'] = this._video.map((v) => v.toJson()).toList();
    }
    data['image_ads_1'] = this._imageAds1;
    data['image_ads_2'] = this._imageAds2;
    data['image_ads_3'] = this._imageAds3;
    data['image_ads_4'] = this._imageAds4;
    data['image_ads_5'] = this._imageAds5;
    data['ads_link_1'] = this._adsLink1;
    data['ads_link_2'] = this._adsLink2;
    data['ads_link_3'] = this._adsLink3;
    data['ads_link_4'] = this._adsLink4;
    data['ads_link_5'] = this._adsLink5;
    data['ads_size_1'] = this._adsSize1;
    data['ads_size_2'] = this._adsSize2;
    data['ads_size_3'] = this._adsSize3;
    data['ads_size_4'] = this._adsSize4;
    data['ads_size_5'] = this._adsSize5;
    return data;
  }
}

class Headline {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postImageContent;
  String _categoryName;
  String _author;

  Headline(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String postImageContent,
      String categoryName,
      String author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postImageContent = postImageContent;
    this._categoryName = categoryName;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get author => _author;
  set author(String author) => _author = author;

  Headline.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postImageContent = json['post_image_content'];
    _categoryName = json['category_name'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_image_content'] = this._postImageContent;
    data['category_name'] = this._categoryName;
    data['author'] = this._author;
    return data;
  }
}

class Recent {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _slug;
  String _categoryName;
  String _postImageContent;
  String _author;

  Recent(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String slug,
      String categoryName,
      String postImageContent,
      String author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._slug = slug;
    this._categoryName = categoryName;
    this._postImageContent = postImageContent;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get author => _author;
  set author(String author) => _author = author;

  Recent.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _slug = json['slug'];
    _categoryName = json['category_name'];
    _postImageContent = json['post_image_content'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['slug'] = this._slug;
    data['category_name'] = this._categoryName;
    data['post_image_content'] = this._postImageContent;
    data['author'] = this._author;
    return data;
  }
}

class Popular {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _slug;
  String _postImageContent;
  String _categoryName;
  String _hitsCounter;
  String _author;

  Popular(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String slug,
      String postImageContent,
      String categoryName,
      String hitsCounter,
      String author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._slug = slug;
    this._postImageContent = postImageContent;
    this._categoryName = categoryName;
    this._hitsCounter = hitsCounter;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get hitsCounter => _hitsCounter;
  set hitsCounter(String hitsCounter) => _hitsCounter = hitsCounter;
  String get author => _author;
  set author(String author) => _author = author;

  Popular.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _slug = json['slug'];
    _postImageContent = json['post_image_content'];
    _categoryName = json['category_name'];
    _hitsCounter = json['hits_counter'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['slug'] = this._slug;
    data['post_image_content'] = this._postImageContent;
    data['category_name'] = this._categoryName;
    data['hits_counter'] = this._hitsCounter;
    data['author'] = this._author;
    return data;
  }
}

class Persib {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _slug;
  String _categoryName;
  String _postImageContent;
  String _author;

  Persib(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String slug,
      String categoryName,
      String postImageContent,
      String author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._slug = slug;
    this._categoryName = categoryName;
    this._postImageContent = postImageContent;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get author => _author;
  set author(String author) => _author = author;

  Persib.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _slug = json['slug'];
    _categoryName = json['category_name'];
    _postImageContent = json['post_image_content'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['slug'] = this._slug;
    data['category_name'] = this._categoryName;
    data['post_image_content'] = this._postImageContent;
    data['author'] = this._author;
    return data;
  }
}

class Netizen {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _slug;
  String _categoryName;
  String _postImageContent;
  Null _author;

  Netizen(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String slug,
      String categoryName,
      String postImageContent,
      Null author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._slug = slug;
    this._categoryName = categoryName;
    this._postImageContent = postImageContent;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  Null get author => _author;
  set author(Null author) => _author = author;

  Netizen.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _slug = json['slug'];
    _categoryName = json['category_name'];
    _postImageContent = json['post_image_content'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['slug'] = this._slug;
    data['category_name'] = this._categoryName;
    data['post_image_content'] = this._postImageContent;
    data['author'] = this._author;
    return data;
  }
}

class Photo {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postImageContent;

  Photo(
      {String postId,
      String postDate,
      String postDateCreated,
      String postTitle,
      String postImageContent}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postImageContent = postImageContent;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;

  Photo.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postImageContent = json['post_image_content'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_image_content'] = this._postImageContent;
    return data;
  }
}

class Video {
  String _videoId;
  String _date;
  String _title;
  String _slug;
  String _youtube;
  String _source;
  String _video;
  String _editor;

  Video(
      {String videoId,
      String date,
      String title,
      String slug,
      String youtube,
      String source,
      String video,
      String editor}) {
    this._videoId = videoId;
    this._date = date;
    this._title = title;
    this._slug = slug;
    this._youtube = youtube;
    this._source = source;
    this._video = video;
    this._editor = editor;
  }

  String get videoId => _videoId;
  set videoId(String videoId) => _videoId = videoId;
  String get date => _date;
  set date(String date) => _date = date;
  String get title => _title;
  set title(String title) => _title = title;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get youtube => _youtube;
  set youtube(String youtube) => _youtube = youtube;
  String get source => _source;
  set source(String source) => _source = source;
  String get video => _video;
  set video(String video) => _video = video;
  String get editor => _editor;
  set editor(String editor) => _editor = editor;

  Video.fromJson(Map<String, dynamic> json) {
    _videoId = json['video_id'];
    _date = json['date'];
    _title = json['title'];
    _slug = json['slug'];
    _youtube = json['youtube'];
    _source = json['source'];
    _video = json['video'];
    _editor = json['editor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['video_id'] = this._videoId;
    data['date'] = this._date;
    data['title'] = this._title;
    data['slug'] = this._slug;
    data['youtube'] = this._youtube;
    data['source'] = this._source;
    data['video'] = this._video;
    data['editor'] = this._editor;
    return data;
  }
}
