class Banner {
  String _status;
  int _kode;
  String _topBanner;
  double _adsSize;
  String _adsLink1;

  Banner(
      {String status,
        int kode,
        String topBanner,
        double adsSize,
        String adsLink1}) {
    this._status = status;
    this._kode = kode;
    this._topBanner = topBanner;
    this._adsSize = adsSize;
    this._adsLink1 = adsLink1;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  String get topBanner => _topBanner;
  set topBanner(String topBanner) => _topBanner = topBanner;
  double get adsSize => _adsSize;
  set adsSize(double adsSize) => _adsSize = adsSize;
  String get adsLink1 => _adsLink1;
  set adsLink1(String adsLink1) => _adsLink1 = adsLink1;

  Banner.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    _topBanner = json['topBanner'];
    _adsSize = json['ads_size'];
    _adsLink1 = json['ads_link_1'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    data['topBanner'] = this._topBanner;
    data['ads_size'] = this._adsSize;
    data['ads_link_1'] = this._adsLink1;
    return data;
  }
}
