class Headline {
  String _postId;
  String _postDate;
  String _postDateCreated;
  String _postTitle;
  String _postImageContent;
  String _categoryName;
  String _author;

  Headline(
      {String postId,
        String postDate,
        String postDateCreated,
        String postTitle,
        String postImageContent,
        String categoryName,
        String author}) {
    this._postId = postId;
    this._postDate = postDate;
    this._postDateCreated = postDateCreated;
    this._postTitle = postTitle;
    this._postImageContent = postImageContent;
    this._categoryName = categoryName;
    this._author = author;
  }

  String get postId => _postId;
  set postId(String postId) => _postId = postId;
  String get postDate => _postDate;
  set postDate(String postDate) => _postDate = postDate;
  String get postDateCreated => _postDateCreated;
  set postDateCreated(String postDateCreated) =>
      _postDateCreated = postDateCreated;
  String get postTitle => _postTitle;
  set postTitle(String postTitle) => _postTitle = postTitle;
  String get postImageContent => _postImageContent;
  set postImageContent(String postImageContent) =>
      _postImageContent = postImageContent;
  String get categoryName => _categoryName;
  set categoryName(String categoryName) => _categoryName = categoryName;
  String get author => _author;
  set author(String author) => _author = author;

  Headline.fromJson(Map<String, dynamic> json) {
    _postId = json['post_id'];
    _postDate = json['post_date'];
    _postDateCreated = json['post_date_created'];
    _postTitle = json['post_title'];
    _postImageContent = json['post_image_content'];
    _categoryName = json['category_name'];
    _author = json['author'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_id'] = this._postId;
    data['post_date'] = this._postDate;
    data['post_date_created'] = this._postDateCreated;
    data['post_title'] = this._postTitle;
    data['post_image_content'] = this._postImageContent;
    data['category_name'] = this._categoryName;
    data['author'] = this._author;
    return data;
  }
}