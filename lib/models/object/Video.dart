class Video {
  String _videoId;
  String _date;
  String _title;
  String _slug;
  String _youtube;
  String _source;
  String _video;
  String _editor;

  Video(
      {String videoId,
        String date,
        String title,
        String slug,
        String youtube,
        String source,
        String video,
        String editor}) {
    this._videoId = videoId;
    this._date = date;
    this._title = title;
    this._slug = slug;
    this._youtube = youtube;
    this._source = source;
    this._video = video;
    this._editor = editor;
  }

  String get videoId => _videoId;
  set videoId(String videoId) => _videoId = videoId;
  String get date => _date;
  set date(String date) => _date = date;
  String get title => _title;
  set title(String title) => _title = title;
  String get slug => _slug;
  set slug(String slug) => _slug = slug;
  String get youtube => _youtube;
  set youtube(String youtube) => _youtube = youtube;
  String get source => _source;
  set source(String source) => _source = source;
  String get video => _video;
  set video(String video) => _video = video;
  String get editor => _editor;
  set editor(String editor) => _editor = editor;

  Video.fromJson(Map<String, dynamic> json) {
    _videoId = json['video_id'];
    _date = json['date'];
    _title = json['title'];
    _slug = json['slug'];
    _youtube = json['youtube'];
    _source = json['source'];
    _video = json['video'];
    _editor = json['editor'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['video_id'] = this._videoId;
    data['date'] = this._date;
    data['title'] = this._title;
    data['slug'] = this._slug;
    data['youtube'] = this._youtube;
    data['source'] = this._source;
    data['video'] = this._video;
    data['editor'] = this._editor;
    return data;
  }
}