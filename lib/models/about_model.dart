class AboutModel {
  String _status;
  int _kode;
  List<Data> _data;

  AboutModel({String status, int kode, List<Data> data}) {
    this._status = status;
    this._kode = kode;
    this._data = data;
  }

  String get status => _status;
  set status(String status) => _status = status;
  int get kode => _kode;
  set kode(int kode) => _kode = kode;
  List<Data> get data => _data;
  set data(List<Data> data) => _data = data;

  AboutModel.fromJson(Map<String, dynamic> json) {
    _status = json['status'];
    _kode = json['kode'];
    if (json['data'] != null) {
      _data = new List<Data>();
      json['data'].forEach((v) {
        _data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this._status;
    data['kode'] = this._kode;
    if (this._data != null) {
      data['data'] = this._data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  String _title;
  String _content;
  String _img;

  Data({String title, String content, String img}) {
    this._title = title;
    this._content = content;
    this._img = img;
  }

  String get title => _title;
  set title(String title) => _title = title;
  String get content => _content;
  set content(String content) => _content = content;
  String get img => _img;
  set img(String img) => _img = img;

  Data.fromJson(Map<String, dynamic> json) {
    _title = json['title'];
    _content = json['content'];
    _img = json['img'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this._title;
    data['content'] = this._content;
    data['img'] = this._img;
    return data;
  }
}
